import { OrderDetailsFeature } from "@/feature/order-details";

export interface OrderDetailsViewProps {
    navigation: any
    route: any
}

export default function OrderDetailsView(props: OrderDetailsViewProps) {

    return (
        <OrderDetailsFeature {...props} />
    )
};
