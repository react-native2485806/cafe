import { useEffect, useRef, useState } from "react";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { DiscoveryStackView } from "../discovery";
import { CatalogStackView } from "../catalog";
import { Ionicons } from "@expo/vector-icons";
import { SimpleLineIcons } from '@expo/vector-icons';
import { Alert, ScrollView, StyleSheet, Text, View } from "react-native";
import { AntDesign } from '@expo/vector-icons';
import { PrintStackView } from "../print";
import DataContext from "@/app/data-context";
import ThermalPrinterModule from 'react-native-thermal-printer';
import ViewShot, { captureRef } from 'react-native-view-shot';
import { itemSales } from "@/util/temp";
import { useDispatch, useSelector } from "react-redux";
import { updatePrinterLanguage, updateRefreshCatalog } from "@/feature/catalog/redux/catalogReducer";
import { usePrintHook } from "@/feature/print/hooks/usePrint.hook";
import UUID from 'react-native-uuid';
import { RootState } from "@/config/redux";
import { SelectedMenu } from "@/config/type";

const Tab = createBottomTabNavigator();

export default function DashboardView() {

    const dispatch = useDispatch();
    const [uuid, setUUID] = useState<string>(UUID.v4().toString())
    const { selectedMenu, total, printerSettings: { printingLanguage } } = useSelector((state: RootState) => state.catalogReducer)
    const [localSelectedMenu, setLocalSelectedMenu] = useState<SelectedMenu[]>([])
    const [localTotal, setLocalTotal] = useState<number>(0)

    const { onClickPrint } = usePrintHook({})

    useEffect(() => {
        console.log(ThermalPrinterModule?.getBluetoothDeviceList())
    })

    useEffect(() => {
        if (selectedMenu.length) {
            setLocalSelectedMenu(selectedMenu);
            setLocalTotal(total)
        }
    }, [selectedMenu])


    const ratio = 1
    const h = 256 * ratio
    const width_ratio = 1.504
    const headerRef = useRef<any>();
    const firstBatchDataRef = useRef<any>();
    const secondBatchDataRef = useRef<any>();
    const thirdBatchDataRef = useRef<any>();
    const summaryRef = useRef<any>();

    return (
        <>
            <Tab.Navigator
                screenOptions={{
                    headerShown: false,
                }}
            >
                <Tab.Screen
                    name="Home"
                    component={CatalogStackView}
                    options={{ tabBarIcon: ({ color, size }) => <Ionicons style={{}} name="home" color={color} size={size} /> }}
                />
                <Tab.Screen
                    name="Print "
                    component={PrintStackView}
                    listeners={{
                        tabPress: async (e) => {
                            let uuid = UUID.v4().toString()
                            setUUID(uuid)
                            e.preventDefault()
                            onClickPrint(uuid, headerRef, firstBatchDataRef, secondBatchDataRef, thirdBatchDataRef, summaryRef, localSelectedMenu, setLocalSelectedMenu)
                        },
                        tabLongPress: () => {
                            dispatch(updatePrinterLanguage(printingLanguage == 'bn' ? 'en' : 'bn'))
                            alert(`${printingLanguage === 'bn' ? "Printer language sated to English!" : "বাংলা ভাষায় প্রিন্টার সেট করা হয়েছে!"}`)
                        }
                    }}
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <View style={styles.container} pointerEvents="box-none">
                                <View style={styles.button}>
                                    <SimpleLineIcons name="printer" style={styles.buttonIcon} />
                                </View>
                            </View>
                        )
                    }}
                />
                <Tab.Screen
                    name="Discovery "
                    component={DiscoveryStackView}
                    options={{ tabBarIcon: ({ color, size }) => <AntDesign name="barschart" size={size} color={color} /> }}
                />
            </Tab.Navigator>

            {/* 385 X 256 = 1.504:1*/}
            <ViewShot ref={headerRef} options={{ fileName: "header", format: "png", quality: 1 }} style={{ width: 235 * 9, height: 520, backgroundColor: "white", borderBottomColor: "black", borderBottomWidth: 10, position: "absolute", zIndex: -1 }}>
                <Text style={{ fontSize: 13 * 7, fontWeight: "800" }}>                  Vista Cafe</Text>
                <Text style={{ fontSize: 10 * 7 }}>                 নোবিপ্রবি, শান্তিনিকেতন।</Text>
                <Text style={{ fontSize: 10 * 7 }}>           {new Date().toDateString() + "; " + new Date().toLocaleTimeString()}</Text>
                <Text style={{ fontSize: 10 * 7 }}>Order Id # <Text style={{ fontSize: 10 * 7, fontWeight: "700" }}>{uuid.substring(uuid.length - 12, uuid.length)}</Text></Text>
                <View style={{ flexDirection: "row", width: 1250 }}>
                    <Text style={{ fontSize: 10 * 7, fontWeight: "800", width: 200, textAlign: "left" }}>Qty</Text>
                    <Text style={{ fontSize: 10 * 7, fontWeight: "800", width: 700, textAlign: "left" }}>Item</Text>
                    <Text style={{ fontSize: 10 * 7, fontWeight: "800", width: 340, textAlign: "right" }}>Price</Text>
                </View>
            </ViewShot>

            {/* 3 batch */}
            <ViewShot ref={firstBatchDataRef} options={{ fileName: "header", format: "png", quality: 1 }} style={{ width: 235 * 9, height: 320, backgroundColor: "white", borderBottomColor: "black", borderBottomWidth: selectedMenu.length <= 3 ? 10 : 0, position: "absolute", zIndex: -1 }}>
                {localSelectedMenu.length <= 3 && localSelectedMenu?.map((item) =>
                    <View style={{ flexDirection: "row", width: 1250 }}>
                        <Text style={{ fontSize: 10 * 7, width: 200, textAlign: "left" }}>{item.quantity} X </Text>
                        <Text style={{ fontSize: 10 * 7, width: 700, textAlign: "left" }}>{item.nameBn.substring(0, 13)}{item.nameBn.length > 13 && '...'}</Text>
                        <Text style={{ fontSize: 10 * 7, width: 340, textAlign: "right" }}>&#2547; {(item.quantity * item.price).toFixed(2)}</Text>
                    </View>
                )}
                {localSelectedMenu.length <= 11 && localSelectedMenu.length > 8 && localSelectedMenu?.map((item, index) => index < 3 &&
                    <View style={{ flexDirection: "row", width: 1250 }}>
                        <Text style={{ fontSize: 10 * 7, width: 200, textAlign: "left" }}>{item.quantity} X </Text>
                        <Text style={{ fontSize: 10 * 7, width: 700, textAlign: "left" }}>{item.nameBn.substring(0, 13)}{item.nameBn.length > 13 && '...'}</Text>
                        <Text style={{ fontSize: 10 * 7, width: 340, textAlign: "right" }}>&#2547; {(item.quantity * item.price).toFixed(2)}</Text>
                    </View>
                )}
            </ViewShot>

            {/* 5 batch */}
            <ViewShot ref={secondBatchDataRef} options={{ fileName: "header", format: "png", quality: 1 }} style={{ width: 235 * 9, height: 520, backgroundColor: "white", borderBottomColor: "black", borderBottomWidth: localSelectedMenu.length <= 5 && localSelectedMenu.length > 3 ? 10 : 0, position: "absolute", zIndex: -1 }}>
                {localSelectedMenu.length <= 5 && localSelectedMenu.length > 3 && localSelectedMenu?.map((item) =>
                    <View style={{ flexDirection: "row", width: 1250 }}>
                        <Text style={{ fontSize: 10 * 7, width: 200, textAlign: "left" }}>{item.quantity} X </Text>
                        <Text style={{ fontSize: 10 * 7, width: 700, textAlign: "left" }}>{item.nameBn.substring(0, 13)}{item.nameBn.length > 13 && '...'}</Text>
                        <Text style={{ fontSize: 10 * 7, width: 340, textAlign: "right" }}>&#2547; {(item.quantity * item.price).toFixed(2)}</Text>
                    </View>
                )}
                {localSelectedMenu.length <= 13 && localSelectedMenu.length > 11 && localSelectedMenu?.map((item, index) => index < 5 &&
                    <View style={{ flexDirection: "row", width: 1250 }}>
                        <Text style={{ fontSize: 10 * 7, width: 200, textAlign: "left" }}>{item.quantity} X </Text>
                        <Text style={{ fontSize: 10 * 7, width: 700, textAlign: "left" }}>{item.nameBn.substring(0, 13)}{item.nameBn.length > 13 && '...'}</Text>
                        <Text style={{ fontSize: 10 * 7, width: 340, textAlign: "right" }}>&#2547; {(item.quantity * item.price).toFixed(2)}</Text>
                    </View>
                )}
            </ViewShot>

            {/* 8 batch */}
            <ViewShot ref={thirdBatchDataRef} options={{ fileName: "firstBatch", format: "png", quality: 1 }} style={{ width: h * width_ratio, height: h, backgroundColor: "white", borderBottomColor: "black", borderBottomWidth: 5, position: "absolute", zIndex: -1 }}>
                {localSelectedMenu.length <= 8 && localSelectedMenu.length > 5 && localSelectedMenu?.map((item) =>
                    <View style={{ flexDirection: "row", width: h * width_ratio }}>
                        <Text style={{ fontSize: 22 * ratio, width: h * width_ratio * .16, textAlign: "left" }}>{item.quantity} X </Text>
                        <Text style={{ fontSize: 22 * ratio, width: h * width_ratio * .56, textAlign: "left" }}>{item.nameBn.substring(0, 13)}{item.nameBn.length > 13 && '...'}</Text>
                        <Text style={{ fontSize: 22 * ratio, width: h * width_ratio * .27, textAlign: "right" }}>&#2547; {(item.quantity * item.price).toFixed(2)}</Text>
                    </View>
                )}
                {localSelectedMenu.length <= 11 && localSelectedMenu.length > 8 && localSelectedMenu?.map((item, index) => index >= 3 &&
                    <View style={{ flexDirection: "row", width: h * width_ratio }}>
                        <Text style={{ fontSize: 22 * ratio, width: h * width_ratio * .16, textAlign: "left" }}>{item.quantity} X </Text>
                        <Text style={{ fontSize: 22 * ratio, width: h * width_ratio * .56, textAlign: "left" }}>{item.nameBn.substring(0, 13)}{item.nameBn.length > 13 && '...'}</Text>
                        <Text style={{ fontSize: 22 * ratio, width: h * width_ratio * .27, textAlign: "right" }}>&#2547; {(item.quantity * item.price).toFixed(2)}</Text>
                    </View>
                )}
                {localSelectedMenu.length <= 13 && localSelectedMenu.length > 11 && localSelectedMenu?.map((item, index) => index >= 5 &&
                    <View style={{ flexDirection: "row", width: h * width_ratio }}>
                        <Text style={{ fontSize: 22 * ratio, width: h * width_ratio * .16, textAlign: "left" }}>{item.quantity} X </Text>
                        <Text style={{ fontSize: 22 * ratio, width: h * width_ratio * .56, textAlign: "left" }}>{item.nameBn.substring(0, 13)}{item.nameBn.length > 13 && '...'}</Text>
                        <Text style={{ fontSize: 22 * ratio, width: h * width_ratio * .27, textAlign: "right" }}>&#2547; {(item.quantity * item.price).toFixed(2)}</Text>
                    </View>
                )}
            </ViewShot>

            {/* Summary */}
            <ViewShot ref={summaryRef} options={{ fileName: "header", format: "png", quality: 1 }} style={{ width: 235 * 9, height: 80, backgroundColor: "white", position: "absolute", zIndex: -1 }}>
                <View style={{ flexDirection: "row", width: 1250 }}>
                    <Text style={{ fontSize: 70, width: 900, textAlign: "left", fontWeight: "800" }}> Total: </Text>
                    <Text style={{ fontSize: 70, width: 340, textAlign: "right", fontWeight: "800" }}>&#2547; {localTotal.toFixed(2)}</Text>
                </View>
            </ViewShot>

        </>
    )
};

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        width: 75,
        alignItems: 'center',
    },
    background: {
        position: 'absolute',
        top: 0,
    },
    button: {
        top: -16,
        justifyContent: 'center',
        alignItems: 'center',
        width: 70,
        height: 70,
        borderRadius: 50,
        backgroundColor: '#E94F37',
    },
    buttonIcon: {
        fontSize: 30,
        color: '#F6F7EB'
    }
});
