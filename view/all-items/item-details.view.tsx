import { ItemDetailsFeature } from "@/feature/all-items";
import { Text, View } from "react-native";


interface ItemDetailsViewProps {
    navigation: any
}

export default function ItemDetailsView(params: ItemDetailsViewProps) {


    return (
        <ItemDetailsFeature navigation={params.navigation}/>
    )
};
