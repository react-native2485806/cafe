import { AllItemCategoryFeature } from "@/feature/all-items"; 

interface AllItemCategoryViewProps {
    navigation: any
}

export default function AllItemCategoryView(params: AllItemCategoryViewProps) {

    return (
        <AllItemCategoryFeature navigation={params.navigation}/>
    )
};
