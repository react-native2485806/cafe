import { AllItemFeature } from "@/feature/all-items";

interface AllItemViewProps {
    navigation: any
    route: any
}

export default function AllItemView(params: AllItemViewProps) {

    return (
        <AllItemFeature {...params}/>
    )
};
