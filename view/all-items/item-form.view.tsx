import { ItemFormFeature } from "@/feature/all-items";


interface ItemFormViewProps {
    navigation: any,
    route: any,
}

export default function ItemFormView(params: ItemFormViewProps) {

    return (
        <ItemFormFeature {...params}/>
    )
};
