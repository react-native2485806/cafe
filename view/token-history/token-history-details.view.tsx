import { TokenHistoryDetailsFeature } from "@/feature/token-history";

export interface TokenHistoryDetailsViewProps{
    navigation: any
    route: any
}

export default function TokenHistoryDetailsView(props: TokenHistoryDetailsViewProps) {
    
    return(
        <TokenHistoryDetailsFeature {...props}/>
    )
};
