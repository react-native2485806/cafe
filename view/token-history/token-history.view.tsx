import { TokenHistoryFeature } from "@/feature/token-history";
import { Text, View } from "react-native";

export interface TokenHistoryViewProps{
    navigation: any
    route:any
}

export default function TokenHistoryView(props:TokenHistoryViewProps) {

    return(
        <TokenHistoryFeature {...props}/>
    )
    
};
