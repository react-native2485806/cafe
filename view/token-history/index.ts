import TokenHistoryView from "./token-history.view"
import TokenHistoryDetailsView from "./token-history-details.view"

export{
    TokenHistoryView,
    TokenHistoryDetailsView
}
