import { createNativeStackNavigator } from "@react-navigation/native-stack";
import PrintView from "./print.view";
import { Text, View } from "react-native";

const Stack = createNativeStackNavigator();

function PrintStackView(params: any) {

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: true,
                header: (props) => {
                    return (
                        <View style={{ backgroundColor: "white", height: 80, justifyContent: "center", alignItems: "center", flexDirection: "row", paddingTop: 15, paddingHorizontal: 30 }}>
                            <Text style={{ fontSize: 18, fontWeight: "700", color: "red" }}>{props.route.name}</Text>
                        </View>
                    )
                }
            }}
        >
            <Stack.Screen
                name="Print"
                component={PrintView}
                options={{}}
            />
        </Stack.Navigator>
    )
};

export {
    PrintStackView
}