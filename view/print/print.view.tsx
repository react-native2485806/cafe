import { PrintFeature } from "@/feature/print";

export interface PrintViewProps {
    navigation: any
    route: any
}

export default function PrintView(props: PrintViewProps) {

    return (
        <PrintFeature {...props} />
    )
};
