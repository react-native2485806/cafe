import { CatalogFeature } from "@/feature/catalog";
import CatalogListFeature from "@/feature/catalog/catalog-list.feature";
import { View } from "react-native";

export interface CatalogListViewProps {
    navigation: any
    route: any
}

export default function CatalogListView(props: CatalogListViewProps) {

    return (
        <View style={{ flex: .9, flexDirection: 'column' }}>
            <View style={{ flex: 9, margin: 10, marginTop: 0, borderRadius: 5 }}>
                <CatalogListFeature {...props} />
            </View>
        </View >
    )
};
