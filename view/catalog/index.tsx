import { createNativeStackNavigator } from "@react-navigation/native-stack";
import CatalogListView from "./catalog-list.view";
import { OrderDetailsView } from "../order-details";
import { Alert, Text, TouchableHighlight, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/config/redux";
import { updateCategory, updateItem, updateItemSale, updateRefreshCatalog, updateToken } from "@/feature/catalog/redux/catalogReducer";
import { AntDesign } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { db } from "@/config/db";

const Stack = createNativeStackNavigator();

export interface CatalogStackViewProps {

}

function CatalogStackView(props: CatalogStackViewProps) {

    const total = useSelector((state: RootState) => state.catalogReducer.total);
    const dispatch = useDispatch();

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: true,
                header: (props) => {
                    return (
                        <View style={{ backgroundColor: "white", height: 60, justifyContent: "space-between", alignItems: "center", flexDirection: "row", paddingHorizontal: 30 }}>
                            <TouchableHighlight
                                activeOpacity={0.6}
                                underlayColor="#DDDDDD"
                                onPress={() => dispatch(updateRefreshCatalog())}
                                // onLongPress={() => { updateDB(); }}
                                style={{ borderRadius: 50, marginLeft: -16, padding: 10, backgroundColor: "#fff8f0" }}>
                                <MaterialCommunityIcons name="menu-swap-outline" size={33} color="red" />
                            </TouchableHighlight>
                            <Text style={{ fontSize: 18, fontWeight: "700", color: "red" }}>{props.route.name}</Text>
                            {/* {"\u0633 \u0633"} */}
                            <Text style={{ fontSize: 18, fontWeight: "700", color: "red" }}>{total}/=</Text>
                        </View>
                    )
                }
            }}
        >
            <Stack.Screen
                name="Vista Cafe"
                component={CatalogListView}
                options={{}}
            />
            <Stack.Screen
                name="Order Details"
                component={OrderDetailsView}
                options={{}}
            />
        </Stack.Navigator>
    )
};

export {
    CatalogListView,
    CatalogStackView
}