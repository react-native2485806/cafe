import ItemSalesView from "./item-sales.view"
import ItemSalesDetailsView from "./item-sales-details.view"

export{
    ItemSalesView,
    ItemSalesDetailsView
}
