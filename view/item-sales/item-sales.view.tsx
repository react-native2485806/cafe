import { ItemSalesFeature } from "@/feature/item-sales";
import { Text } from "@rneui/base";
import { View } from "react-native";


export interface ItemSalesViewProps {
    navigation: any
    route:any
}

export default function ItemSalesView(props: ItemSalesViewProps) {

    return (
        <ItemSalesFeature {...props} />
    )

};
