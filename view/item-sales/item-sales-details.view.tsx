import { ItemSalesDetailsFeature } from "@/feature/item-sales";

export interface ItemSalesDetailsViewProps {
    navigation: any
    route: any
}

export default function ItemSalesDetailsView(props: ItemSalesDetailsViewProps) {

    return (
        <ItemSalesDetailsFeature {...props} />
    )
};
