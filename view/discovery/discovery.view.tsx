import { DiscoveryFeature } from "@/feature/discovery";

export interface DiscoveryViewProps {
    navigation: any
}

export default function DiscoveryView(props: DiscoveryViewProps) {

    return (
        <DiscoveryFeature {...props}/>
    )

};
