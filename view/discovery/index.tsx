import DiscoveryView from "./discovery.view";
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { TokenHistoryDetailsView, TokenHistoryView } from "../token-history";
import { ItemSalesDetailsView, ItemSalesView } from "../item-sales";
import CashStackView from "../cash";
import { Text, View } from "react-native";
import { AllItemStackView } from "../all-items";
import { DeviceConnectionView } from "../device-connection";
import { DailySheetView } from "../daily-sheet";
import ExpenseStackView from "../expense";
import ExpenseView from "../expense/expense.view";

const Stack = createNativeStackNavigator();

export interface DiscoveryViewProps {

}

function DiscoveryStackView(props: DiscoveryViewProps) {

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: true,
                header: (props) => {
                    let spaeBetween= props.route.name == "Token History" || props.route.name == "Item Sales";
                    return (
                        props.route.name !== "All Items" && props.route.name !== "Expense" &&
                        <View style={{ backgroundColor: "white", height: 60,  alignItems: "center", justifyContent: spaeBetween ? "space-around":"center", flexDirection: "row" }}>
                            <Text style={{ fontSize: 18, fontWeight: "700", color: "red" }}>{props.route.name}</Text>
                        </View>
                    )
                }
            }}
        >
            <Stack.Screen
                name="Discovery"
                component={DiscoveryView}
                options={{}}
            />
            <Stack.Screen
                name="Token History"
                component={TokenHistoryView}
                options={{}}
            />
            <Stack.Screen
                name="Token History Details"
                component={TokenHistoryDetailsView}
                options={{}}
            />
            <Stack.Screen
                name="Item Sales"
                component={ItemSalesView}
            />
            <Stack.Screen
                name="Item Sales Details"
                component={ItemSalesDetailsView}
            />
            <Stack.Screen
                name="Cash"
                component={CashStackView}
            />
            <Stack.Screen
                name="All Items"
                component={AllItemStackView}
            />
            <Stack.Screen
                name="Files"
                component={ItemSalesDetailsView}
            />
            <Stack.Screen
                name="File Details"
                component={CashStackView}
            />
            <Stack.Screen
                name="Device Connection"
                component={DeviceConnectionView}
            />
            <Stack.Screen
                name="Daily Sheet"
                component={DailySheetView}
            />
            <Stack.Screen
                name="Expense"
                component={ExpenseStackView}
            />
            
            <Stack.Screen
                name="Expenses"
                component={ExpenseView}
            />
        </Stack.Navigator>
    )
};

export {
    DiscoveryStackView
}
