import { LoginFeature } from "@/feature/auth";

export interface LoginViewProps {
    onPress: (userName: string, password: string) => void;
    onKeyPress: (num: string) => void;
    pass: string;
    warning: boolean;
    setSaveMode: (mode: boolean) => void
    saveMode: boolean
}

export default function LoginView(props: LoginViewProps) {


    return (
        <>
            <LoginFeature {...props} />
        </>
    )

};
