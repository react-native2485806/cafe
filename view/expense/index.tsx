import ExpenseView from "./expense.view"


import { Alert, Text, TouchableHighlight, View } from "react-native";
// import AllItemView from "./all-item.view"
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { AllItemFeature } from "@/feature/all-items";
// import AllItemCategoryView from "./all-item-category.view";
// import ItemDetailsView from "./item-details.view";
// import ItemFormView from "./item-form.view";
import { AntDesign } from "@expo/vector-icons";
import { FontAwesome6 } from '@expo/vector-icons';
import ExpenseDetailsView from "./expense-details.view";
import SaveExpenseView from "./save-expense.view";

const Stack = createNativeStackNavigator();

export default function ExpenseStackView() {

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: true,
                header: (props) => {
                    return (
                        <View style={{ backgroundColor: "white", height: 60, justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                            <Text style={{ fontSize: 18, fontWeight: "700", color: "red" }}>{props.route.name}</Text>
                        </View>
                    )
                }
            }}>
            <Stack.Screen
                name="Expenses"
                component={ExpenseView}

                options={{
                    header: (props) => {
                        return (
                            <View style={{ backgroundColor: "white", height: 60, justifyContent: "center", alignItems: "center", flexDirection: "row" }}>
                                <Text style={{ fontSize: 18, fontWeight: "700", color: "red" }}>{props.route.name}</Text>
                                <TouchableHighlight
                                    activeOpacity={0.6}
                                    underlayColor="#DDDDDD"
                                    onPress={() => props.navigation.navigate("Save Expense")}
                                    style={{ borderRadius: 60, position: "absolute", right: 30, top: 15, alignItems: "center" }}>
                                    <FontAwesome6 name="add" size={30} color="#4493b8" style={{}} />
                                </TouchableHighlight>
                            </View>
                        )
                    }
                }}
            />

            <Stack.Screen
                name="Expense Details"
                component={ExpenseDetailsView}
                options={{}}
            />
            <Stack.Screen
                name="Save Expense"
                component={SaveExpenseView}
                options={{}}
            />

        </Stack.Navigator>
    )
}




// export {
//     ExpenseView
// }
