import { SaveExpenseFeature } from "@/feature/expense";
import { Text, View } from "react-native";


export interface SaveExpenseViewProps {

    navigation: any,
    route: any,
}

export default function SaveExpenseView(props: SaveExpenseViewProps) {

    return (
        <SaveExpenseFeature {...props} />
    )

};
