import { ExpenseFeature } from "@/feature/expense";
import { Text, View } from "react-native";


export interface ExpenseViewProps {
    navigation: any
    route:any
}

export default function ExpenseView(props: ExpenseViewProps) {

    return (
        <ExpenseFeature {...props}/>
    )

};
