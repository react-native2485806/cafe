import { FlatList, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Avatar, ListItem } from '@rneui/themed';
import { useEffect, useState } from "react";
import { collection, onSnapshot, query } from "firebase/firestore";
import { firestore } from "@/config/firebaseConfig";
import { ItemSale } from "@/config/type";

export interface SalesItemListComponentProps {
    data: any[]

}

export default function SalesItemListComponent(params: SalesItemListComponentProps) {

    const { data } = params
    const [index, setIndex] = useState(0);

    const [category, setCategory] = useState<any[]>([])
    const [total, setTotal] = useState(0)
    const [categorizedItem, setCategorizedItem] = useState<any[]>(data?.filter((item: ItemSale) => category[index]?.uuid == item?.categoryId) ?? [])

    useEffect(() => {
        const cq = query(collection(firestore, "Category"));
        onSnapshot(cq, { includeMetadataChanges: true }, (snapshot) => {
            const data = snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    name: i.data().name,
                    nameBn: i.data().nameBn,
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })
            setCategory(data)
        });

    }, [])


    useEffect(() => {
        setCategorizedItem(
            data?.filter((item: ItemSale) => category[index]?.uuid == item?.categoryId)
        )

    }, [index, data, category])

    useEffect(() => {
        let t = 0;
        for (let i of categorizedItem) {
            t = t + (i.price * i.quantity)
        }
        setTotal(t)

    }, [categorizedItem])

    console.log(total)

    return (
        <>
            <View style={{ display: "flex", flexDirection: "row", maxHeight: 73, borderRadius: 5, backgroundColor: "white", padding: 5, marginTop: 5, marginBottom: 8 }}>

                <View style={{ justifyContent: "center", paddingHorizontal: 7, width:"28%", borderColor:"#db5000", borderWidth:1, marginVertical:5, borderRadius:5 }}>
                    <Text onPress={() => { }} style={{ color: "#db5000", textAlign: "center", fontSize: 22, fontWeight: "600" }}>
                    &#2547; {total}
                    </Text>
                </View>
                <ScrollView
                    style={{}}
                    horizontal={true}
                    contentContainerStyle={styles.tabBar}
                    showsHorizontalScrollIndicator={false}
                >
                    {category.map((cat: any, i: any) => (
                        <TouchableOpacity
                            key={cat.uuid}
                            onPress={() => { setIndex(i) }}
                            style={[
                                styles.tabItem,
                                index === i && styles.selectedTabItem,
                            ]}
                        >
                            <Text style={{ color: "white" }}>{cat.nameBn}</Text>
                        </TouchableOpacity>
                    ))}
                </ScrollView>
            </View>
            {/* <View onTouchEnd={() => { }} style={{ display: "flex", flexDirection: "row", width: "100%", paddingHorizontal: 20, paddingVertical: 15, borderRadius: 5, backgroundColor: "#fff" }}>
                <View style={{ flex: 0.1, borderRadius: 5 }}>
                    <Text onPress={() => { }} style={{ color: "#aaa", textAlign: "left", fontSize: 18, fontWeight: "700" }}>

                    </Text>
                </View>
                <View style={{ flex: 0.2, borderRadius: 5 }}>
                    <Text onPress={() => { }} style={{ color: "#888", textAlign: "left", fontSize: 18, fontWeight: "700" }}>
                        Total
                    </Text>
                </View>
                <View style={{ flex: 0.4, borderRadius: 5 }}>
                    <Text onPress={() => { }} style={{ color: "#ddd", textAlign: "center", fontSize: 18, fontWeight: "700" }}>
                        {category[index]?.nameBn}
                    </Text>
                </View>
                <View style={{ flex: 0.1, borderRadius: 5 }}>
                    <Text onPress={() => { }} style={{ color: "#888", textAlign: "left", fontSize: 18, fontWeight: "700" }}>
                    </Text>
                </View>
                <View style={{ flex: 0.2, borderRadius: 5 }}>
                    <Text onPress={() => { }} style={{ color: "#888", textAlign: "left", fontSize: 18, fontWeight: "700" }}>
                        &#2547; {total}
                    </Text>
                </View>
            </View> */}
            <View>
                <FlatList
                    data={categorizedItem}
                    // data={data?.filter((item: ItemSale) => category[index]?.uuid == item?.categoryId)}
                    renderItem={({ item }) => (
                        <>
                            <ListItem key={item.id} style={{ marginVertical: 5 }} onPress={() => { }}>
                                <Avatar
                                    icon={{
                                        // name: "attach-money",
                                        name: "sell",
                                        type: "material",
                                        size: 26,
                                        color: "#5a888f"
                                    }}
                                // containerStyle={{ backgroundColor: "#c2c2c2" }}
                                />

                                <ListItem.Content>
                                    <ListItem.Title>{item.itemNameBn}</ListItem.Title>
                                </ListItem.Content>
                                <View style={{ display: "flex", flexDirection: "row" }}>
                                    <Text style={{ width: 25 }}>{item.quantity}</Text>
                                    <Text style={{ width: 15 }}>X</Text>
                                    <Text style={{ width: 60 }}>{item.price}</Text>
                                    <Text style={{ width: 10 }}>=</Text>
                                    <Text style={{ width: 65 }}>{item.quantity * item.price}/=</Text>
                                </View>
                            </ListItem>

                        </>
                    )}
                />
            </View>
        </>
    )
};


const styles = StyleSheet.create({
    input: {
        height: 50,
        margin: 12,
        borderWidth: 1,
        padding: 2,
        color: "red",
        width: 50,
        marginVertical: -15,
        marginHorizontal: 5,
        fontSize: 22,
        borderColor: "#ffe8e3",
        borderRadius: 5,
        textAlign: "center"
    },



    scene: {
        flex: 1,
    },
    tabBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white",
        minWidth: "72%",
        borderRadius: 5,
        maxHeight: 60,
        padding: 5,
    },
    tabItem: {
        paddingVertical: 12,
        paddingHorizontal: 15,
        backgroundColor: "#ffccb5",
        marginHorizontal: 2,
        borderRadius: 5,
        alignItems: "center"
    },
    selectedTabItem: {
        backgroundColor: "#f08554",
    },

    // input: {
    //     height: 40,
    //     margin: 10,
    //     borderWidth: 2,
    //     padding: 2,
    //     color: "red",
    //     width: 50,
    //     marginVertical: -25,
    //     marginHorizontal: 5,
    //     fontSize: 18,
    //     borderColor: "#ffab66",
    //     borderRadius: 5,
    //     textAlign: "center",
    //     fontWeight: "800"
    // },
})