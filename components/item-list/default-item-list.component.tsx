import { Alert, FlatList, Text, TouchableHighlight } from "react-native";
import { Avatar, ListItem } from '@rneui/themed';
import { Entypo } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { FontAwesome6 } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';

export interface DefaultItemListComponentProps {
    data: any[]
    navigation?: any
    edit?: boolean
    to: string
}

export default function DefaultItemListComponent(params: DefaultItemListComponentProps) {

    const { data, navigation, edit, to } = params

    return (
        <FlatList
            data={data}
            contentContainerStyle={{ margin: 10 }}
            renderItem={({ item }) =>
                <ListItem style={{ marginBottom: 5 }} containerStyle={{ borderRadius: 10 }} onPress={() => { navigation.navigate(to, { data: item }) }}>
                    {/* <Avatar
                        icon={{
                            name: "person-outline",
                            type: "material",
                            size: 26,
                            color: "#5a888f"
                        }}
                    // containerStyle={{ backgroundColor: "#c2c2c2" }}
                    /> */}
                    {/* <AntDesign name="edit" size={30} color="#5a888f" /> */}
                    <Ionicons name="fast-food-outline" size={24} color="#5a888f" />
                    {/* <FontAwesome6 name="bowl-food" size={24} color="#5a888f" /> */}
                    <ListItem.Content>
                        <ListItem.Title>{item.nameBn}</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Title>{item.price}</ListItem.Title>
                    {edit &&
                        <ListItem.Subtitle style={{ display: "flex", alignItems: "center", width: 50, marginVertical: -10 }}>
                            <TouchableHighlight
                                activeOpacity={0.6}
                                underlayColor="#DDDDDD"
                                onPress={() => { navigation.navigate("Edit Item", { data: item }) }}
                                style={{ padding: 10, borderRadius: 50, position: "absolute" }}>
                                {/* <Entypo name="squared-cross" size={30} color="#ffab66" /> */}
                                <AntDesign name="edit" size={30} color="#3e7549" />
                            </TouchableHighlight>
                        </ListItem.Subtitle>
                    }
                    {edit &&
                        <ListItem.Subtitle style={{ display: "flex", alignItems: "center", width: 50, marginVertical: -10 }}>
                            <TouchableHighlight
                                activeOpacity={0.6}
                                underlayColor="#DDDDDD"
                                onPress={() => Alert.alert("pre")}
                                style={{ padding: 10, borderRadius: 50, backgroundColor: "#e6e6e6", position: "absolute" }}>
                                <Entypo name="squared-cross" size={30} color="#ffab66" />
                                {/* <AntDesign name="edit" size={30} color="#ffab66" /> */}
                            </TouchableHighlight>
                        </ListItem.Subtitle>
                    }
                </ListItem>
            }
        />
    )
};
