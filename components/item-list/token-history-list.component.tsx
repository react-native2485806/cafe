import { FlatList, StyleSheet, TouchableHighlight } from "react-native";
import { Avatar, ListItem } from '@rneui/themed';
import { useContext, useState } from "react";
import DataContext from "@/app/data-context";
import { AntDesign } from '@expo/vector-icons';
import { db } from "@/config/db";
import { useDispatch } from "react-redux";
import { updateItemSale, updateItemSalesOfToken, updateLoading, updateToken } from "@/feature/catalog/redux/catalogReducer";
import { deleteData } from "@/config/apis";
import { ItemSale } from "@/config/type";
import { formatAMPM } from "@/util/util";
import { collection, doc, onSnapshot, query, where } from "firebase/firestore";
import { firestore } from "@/config/firebaseConfig";

export interface TokenHistoryListComponentProps {
    data: any[]
    navigation: any,
    revert: number,
}

export default function TokenHistoryListComponent(params: TokenHistoryListComponentProps) {

    const dispatch = useDispatch()
    const { data, navigation, revert } = params
    let list = data

    const setItemSalesOfToken = (tokenId: string) => {
        const categoryDocRef = doc(firestore, "Token", tokenId);
        const q = query(collection(firestore, "ItemSale"), where("tokenId", "==", categoryDocRef));

        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            const data = snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    itemNameBn: i.data().itemNameBn,
                    itemNameEn: i.data().itemNameEn,
                    price: i.data().price,
                    quantity: i.data().quantity,
                    itemId: (i.data().itemId as any)?._key.path.segments[6],
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })
            dispatch(updateItemSalesOfToken(data as any))
        });
    }

    const deleteToken = (uuid: string) => {
        deleteData("Token", uuid)
        const q = query(collection(firestore, "ItemSale"), where("tokenId", "==", doc(firestore, "Token", uuid)));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            snapshot.docs.forEach((i) => {
                deleteData("ItemSale", i.id)
            })
        });
    }

    return (
        <FlatList
            data={list}
            renderItem={({ item, index }) => (
                <ListItem
                    key={item.id}
                    style={{ marginVertical: 5, display: (new Date(item.createdAt) > new Date()) ? "none" : "flex" }}
                    onPress={() => {
                        dispatch(updateLoading(true))
                        setItemSalesOfToken(item.uuid);
                        navigation.navigate('Token History Details', { uuid: item.uuid.substring(item.uuid.length - 12, item.uuid.length), amount: item.amount, createdAt: item.createdAt })
                    }}>
                    <Avatar
                        icon={{
                            name: "generating-tokens",
                            type: "material",
                            size: 26,
                            color: "#5a888f"
                        }}
                    />
                    <ListItem.Title>{list.length - index}.</ListItem.Title>
                    <ListItem.Content>
                        <ListItem.Title style={{ fontSize: 13, color: "#555" }}>{item.uuid.substring(item.uuid.length - 12, item.uuid.length)}</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Content>
                        <ListItem.Title>{formatAMPM(new Date(item.createdAt))}</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Title>{item.amount}/=</ListItem.Title>
                    {
                        revert == 0 &&
                        <TouchableHighlight
                            activeOpacity={0.6}
                            underlayColor="#DDDDDD"
                            onPress={() => deleteToken(item.uuid)}
                            style={{ padding: 15, margin: -15, marginHorizontal: -5, borderRadius: 50, backgroundColor: "#eee" }}>
                            {/* <Entypo name="edit" size={20} color="#ffab66" /> */}
                            <AntDesign name="delete" size={24} color="#ffab66" />
                        </TouchableHighlight>
                    }
                </ListItem>
            )}
        />
    )
};


const styles = StyleSheet.create({
    input: {
        height: 50,
        margin: 12,
        borderWidth: 1,
        padding: 2,
        color: "red",
        width: 50,
        marginVertical: -15,
        marginHorizontal: 5,
        fontSize: 22,
        borderColor: "#ffe8e3",
        borderRadius: 5,
        textAlign: "center"
    },
})