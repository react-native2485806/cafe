import { Alert, FlatList, Text, TouchableHighlight } from "react-native";
import { Avatar, ListItem } from '@rneui/themed';
import { Entypo } from '@expo/vector-icons';
import { Category, Item } from "@/config/type";
import { db } from "@/config/db";
import { useState } from "react";

export interface ItemListComponentProps {
    data: Category;
    allItems: Item[];
    navigation: any;
}

export default function ItemListComponent(params: ItemListComponentProps) {

    const { data, navigation, allItems } = params
    const items = allItems.filter((item: Item) => data.uuid == item.categoryId)
    
    return (
        <FlatList
            data={items}
            renderItem={({ item }) =>
                <ListItem
                    containerStyle={{
                        backgroundColor: "white",
                        borderRadius: 5,
                        
                        height:50,
                    }}
                    style={{ marginBottom: 5, borderRadius: 10 }}
                    onPress={() => { Alert.alert("pre") }}
                >
                    <Avatar
                        icon={{
                            name: "lunch-dining",
                            type: "material",
                            size: 26,
                            color: "#5a888f"
                        }}
                    // containerStyle={{ backgroundColor: "#c2c2c2" }}
                    />
                    <ListItem.Content>
                        <ListItem.Title>{item.nameBn}</ListItem.Title>
                    </ListItem.Content>
                    <Text>{item.price}/=</Text>
                    <TouchableHighlight
                        activeOpacity={0.6}
                        underlayColor="#DDDDDD"
                        onPress={() => navigation.navigate("Edit Item", { data: item })}
                        style={{ padding: 15, margin: -15, marginHorizontal: -5, borderRadius: 50, backgroundColor: "#eee" }}>
                        <Entypo name="edit" size={20} color="#ffab66" />
                    </TouchableHighlight>
                </ListItem>
            }
        />
    )
};

