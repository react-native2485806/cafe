import { FlatList, StyleSheet, TouchableHighlight } from "react-native";
import { Avatar, ListItem } from '@rneui/themed';
import { useContext, useState } from "react";
import DataContext from "@/app/data-context";
import { AntDesign } from '@expo/vector-icons';
import { db } from "@/config/db";
import { useDispatch } from "react-redux";
// import { updateItemSale, updateItemSalesOfExpense, updateLoading, updateExpense } from "@/feature/catalog/redux/catalogReducer";
import { deleteData } from "@/config/apis";
import { ItemSale } from "@/config/type";
import { formatAMPM } from "@/util/util";
import { collection, doc, onSnapshot, query, where } from "firebase/firestore";
import { firestore } from "@/config/firebaseConfig";

export interface ExpenseHistoryListComponentProps {
    data: any[]
    navigation: any,
    revert: number
}

export default function ExpenseHistoryListComponent(params: ExpenseHistoryListComponentProps) {

    const { data, navigation, revert } = params
    let list = data


    const deleteExpense = (uuid: string) => {
        deleteData("Expense", uuid)
    }

    return (
        <FlatList
            data={list}
            renderItem={({ item, index }) => (
                <ListItem
                    key={item.id}
                    style={{ marginVertical: 5, display: (new Date(item.createdAt) > new Date()) ? "none" : "flex" }}
                    onPress={() => {
                        // dispatch(updateLoading(true))
                        // setItemSalesOfExpense(item.uuid);
                        // navigation.navigate('Expense History Details', { uuid: item.uuid, amount: item.amount })
                    }}>
                    <AntDesign name="export" size={26} color="#5a888f" />
                    <ListItem.Title>{list.length - index}.</ListItem.Title>
                    <ListItem.Content>
                        <ListItem.Title>{item.titleBn}</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Content>
                        <ListItem.Title>{formatAMPM(new Date(item.createdAt))}</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Title>{item.amount}/=</ListItem.Title>
                    {
                        revert == 0 &&
                        <TouchableHighlight
                            activeOpacity={0.6}
                            underlayColor="#DDDDDD"
                            onPress={() => deleteExpense(item.uuid)}
                            style={{ padding: 15, margin: -15, marginHorizontal: -5, borderRadius: 50, backgroundColor: "#eee" }}>
                            {/* <Entypo name="edit" size={20} color="#ffab66" /> */}
                            <AntDesign name="delete" size={24} color="#ffab66" />
                        </TouchableHighlight>
                    }
                </ListItem>
            )}
        />
    )
};


const styles = StyleSheet.create({
    input: {
        height: 50,
        margin: 12,
        borderWidth: 1,
        padding: 2,
        color: "red",
        width: 50,
        marginVertical: -15,
        marginHorizontal: 5,
        fontSize: 22,
        borderColor: "#ffe8e3",
        borderRadius: 5,
        textAlign: "center"
    },
})