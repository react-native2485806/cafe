import { StyleSheet, Text, View } from "react-native";



export default function DefaultInfoComponent(params: any) {


    return (
        <View>

                <View style={styles.menu_header_container}>
                    <Text style={{ ...styles.menu_header, width: "70%" }}>Token Details</Text>
                    <Text style={{ ...styles.menu_header, width: "15%" }}>II</Text>
                    <Text style={{ ...styles.menu_header, width: "15%" }}>II</Text>
                </View>
                <View style={{ paddingHorizontal: 5 }}>
                    <View style={styles.header_container}>
                        <Text style={{ ...styles.header, width: "10%" }}>Qty</Text>
                        <Text style={{ ...styles.header, width: "10%" }}>X</Text>
                        <Text style={{ ...styles.header, width: "55%" }}>Item (Price)</Text>
                        <Text style={{ ...styles.header, width: "25%" }}>Total Price</Text>
                    </View>

                    <View style={styles.data_container} >
                        <Text style={{ ...styles.data, width: "10%" }}>2</Text>
                        <Text style={{ ...styles.data, width: "10%" }}>X</Text>
                        <Text style={{ ...styles.data, width: "55%" }}>Pizza (200)</Text>
                        <Text style={{ ...styles.data, width: "25%" }}>400</Text>
                    </View>

                    <View style={styles.data_container} >
                        <Text style={{ ...styles.data, width: "10%" }}>2</Text>
                        <Text style={{ ...styles.data, width: "10%" }}>X</Text>
                        <Text style={{ ...styles.data, width: "55%" }}>Pizza (200)</Text>
                        <Text style={{ ...styles.data, width: "25%" }}>400</Text>
                    </View>

                    <View style={styles.data_container} >
                        <Text style={{ ...styles.data, width: "10%" }}>2</Text>
                        <Text style={{ ...styles.data, width: "10%" }}>X</Text>
                        <Text style={{ ...styles.data, width: "55%" }}>Pizza (200)</Text>
                        <Text style={{ ...styles.data, width: "25%" }}>400</Text>
                    </View>

                    <View style={styles.data_container} >
                        <Text style={{ ...styles.data, width: "10%" }}>2</Text>
                        <Text style={{ ...styles.data, width: "10%" }}>X</Text>
                        <Text style={{ ...styles.data, width: "55%" }}>Pizza (200)</Text>
                        <Text style={{ ...styles.data, width: "25%" }}>400</Text>
                    </View>

                    <View style={styles.data_container} >
                        <Text style={{ ...styles.data, width: "10%" }}>2</Text>
                        <Text style={{ ...styles.data, width: "10%" }}>X</Text>
                        <Text style={{ ...styles.data, width: "55%" }}>Pizza (200)</Text>
                        <Text style={{ ...styles.data, width: "25%" }}>400</Text>
                    </View>

                    <View style={styles.sub_header_container}>
                        <Text style={{ ...styles.sub_header, width: "75%" }}>Sub Total</Text>
                        <Text style={{ ...styles.header, width: "25%" }}>4,800/=</Text>
                    </View>
                    <View style={styles.sub_header_container}>
                        <Text style={{ ...styles.sub_header, width: "75%" }}>Discount (10%)</Text>
                        <Text style={{ ...styles.header, width: "25%" }}>480/=</Text>
                    </View>
                    <View style={styles.sub_header_container}>
                        <Text style={{ ...styles.sub_header, width: "75%" }}>Total</Text>
                        <Text style={{ ...styles.header, width: "25%" }}>4,320/=</Text>
                    </View>
                </View>
            </View>
    )
};



const styles = StyleSheet.create({
    menu_header_container: {
        display: "flex",
        flexDirection: "row",
        backgroundColor: "white",
        paddingHorizontal: 10,
        paddingVertical: 20,
        // borderRadius: 5,
        marginBottom: 20,
        // justifyContent:"space-between",
    },
    menu_header: {
        // display: "flex",
        fontSize: 18,
        color: "#636363",
        fontWeight: "700",
        alignItems: "center",
        justifyContent: "center"
    },
    header_container: {
        display: "flex",
        flexDirection: "row",
        backgroundColor: "#f76b60",
        padding: 10,
        borderRadius: 5,
        // justifyContent:"space-between",
    },
    header: {
        // display: "flex",
        fontSize: 16,
        color: "white",
        fontWeight: "700",
        alignItems: "center",
        justifyContent: "center",
        padding: "auto"
    },
    data_container: {
        display: "flex",
        flexDirection: "row",
        backgroundColor: "white",
        padding: 10,
        borderRadius: 5,
        // justifyContent:"space-between",
        marginVertical: 4,
    },
    data: {
        // display: "flex",
        fontSize: 14,
        color: "#4d4d4d",
        fontWeight: "500",
        alignItems: "center",
        justifyContent: "center",
    },
    sub_header_container: {
        display: "flex",
        flexDirection: "row",
        backgroundColor: "#ffa29c",
        padding: 10,
        borderRadius: 5,
        marginVertical: 4
        // justifyContent:"space-between",
    },
    sub_header: {
        // display: "flex",
        fontSize: 16,
        color: "white",
        fontWeight: "700",
        alignItems: "center",
        justifyContent: "center",
        padding: "auto"
    },
})