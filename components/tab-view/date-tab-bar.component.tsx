import { Text } from "react-native";
import { StyleSheet, TouchableOpacity } from "react-native";
import { ScrollView } from "react-native";
import { NavigationState, SceneRendererProps } from "react-native-tab-view";
import { NavigatorRoute } from "./types";
import { View } from "react-native";
import { Ionicons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

export interface DateTabBarComponentProps extends SceneRendererProps {
    navigationState: NavigationState<NavigatorRoute>
}

export default function DateTabBarComponent(params: DateTabBarComponentProps) {

    const { navigationState, jumpTo } = params

    return (
        <View style={styles.container}>
            <TouchableOpacity
                onPress={() => { jumpTo((navigationState.routes.length-1).toString())}}//params.jumpTo(route.key)}
                style={styles.left_button}
            >
                {/* <Text style={{ color: "white" }}>abc</Text>*/}
                <Ionicons name="today" size={24} color="white" style={{ padding: 10, backgroundColor: "#f09165", borderRadius: 50 }} />
            </TouchableOpacity>

            <View style={styles.sub_container}>
                <TouchableOpacity
                    onPress={() => { jumpTo((navigationState.index-1).toString()) }}//params.jumpTo(route.key)}
                    style={styles.arrow_button}
                >
                    <AntDesign name="left" size={24} color="white" style={{ padding: 7, backgroundColor: "#f09165", borderRadius: 5 }} />
                </TouchableOpacity>
                <Text style={{ textAlign: "center", color: "white", fontSize: 18, fontWeight: "700" }}>{navigationState.routes[navigationState.index].title}</Text>
                <TouchableOpacity
                    onPress={() => {jumpTo((navigationState.index+1).toString()) }}//params.jumpTo(route.key)}
                    style={styles.arrow_button}
                >
                    <AntDesign name="right" size={24} color="white" style={{ padding: 7, backgroundColor: "#f09165", borderRadius: 5 }} />
                </TouchableOpacity>
            </View>
        </View>
    )

};

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#e87743",
        // width:"100%",
        padding: 2,
        margin: 10,
        display: "flex",
        flexDirection: "row",
        borderRadius: 5,
        // justifyContent:"space-between"
    },

    sub_container: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: "80%"
    },
    left_button: {
        // width: "20%",
        paddingHorizontal: 10,
    },
    arrow_button: {
        paddingHorizontal: 10,
        borderRadius: 5
    },
    scene: {
        flex: 1,
    },
    tabBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white",
        minWidth: "100%",
        borderRadius: 5,
        maxHeight: 60,
        padding: 5,
    },
    tabItem: {
        paddingVertical: 12,
        paddingHorizontal: 15,
        backgroundColor: "#ffccb5",
        marginHorizontal: 2,
        borderRadius: 5,
        alignItems: "center"
    },
    selectedTabItem: {
        backgroundColor: "#f08554",
    },
});