import { TabView } from "react-native-tab-view";
import { DefaultTabViewComponentProps } from "./types";
import DefaultTabBarComponent from "./default-tab-bar.component";

export default function DefaultTabViewComponent(params: DefaultTabViewComponentProps) {

  const { swipeEnabled, navigationState, setIndex, initialLayout, renderScene, dateTab } = params

  return (
    <TabView
      swipeEnabled={swipeEnabled}
      navigationState={navigationState}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      renderTabBar={DefaultTabBarComponent}
    />
  )
};
