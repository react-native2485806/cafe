import { Route } from "react-native-tab-view";
import { Layout, SceneRendererProps } from "react-native-tab-view/lib/typescript/src/types";

export interface TabRoute {
    key: string;
    title: string;
}

export interface NavigatorRoute extends Route {
    key: string;
    title: string;
    // titleBn: string;
    // order: number
}


export interface NavigationState {
    index: number,
    routes: NavigatorRoute[]
}

export interface DefaultTabViewComponentProps {
    swipeEnabled: boolean
    navigationState: NavigationState
    setIndex: React.Dispatch<React.SetStateAction<number>>
    initialLayout?: Partial<Layout> | undefined
    renderScene: ({ route, jumpTo, position }: SceneRendererProps & { route: TabRoute }) => JSX.Element
    dateTab?: boolean
}