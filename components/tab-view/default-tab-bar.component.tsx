import { Text } from "react-native";
import { StyleSheet, TouchableOpacity } from "react-native";
import { ScrollView } from "react-native";
import { NavigationState, SceneRendererProps } from "react-native-tab-view";
import { NavigatorRoute } from "./types";

export interface DefaultTabBarComponentProps extends SceneRendererProps {
    navigationState: NavigationState<NavigatorRoute>
}

export default function DefaultTabBarComponent(params: DefaultTabBarComponentProps) {

    return (
        <ScrollView
            style={{ maxHeight: 73, borderRadius: 5, backgroundColor: "white", padding:5, marginTop:5, marginBottom:8 }}
            horizontal={true}
            contentContainerStyle={styles.tabBar}
            showsHorizontalScrollIndicator={false}
        >
            {params.navigationState.routes.map((route: any, index: any) => (
                <TouchableOpacity
                    key={route.key}
                    onPress={() => params.jumpTo(route.key)}
                    style={[
                        styles.tabItem,
                        params.navigationState.index === index && styles.selectedTabItem,
                    ]}
                >
                    <Text style={{color:"white"}}>{route.title}</Text>
                </TouchableOpacity>
            ))}
        </ScrollView>
    )

};

const styles = StyleSheet.create({
    scene: {
        flex: 1,
    },
    tabBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white",
        minWidth: "100%",
        borderRadius: 5,
        maxHeight: 60,
        padding:5,
    },
    tabItem: {
        paddingVertical: 12,
        paddingHorizontal:15,
        backgroundColor: "#ffccb5",
        marginHorizontal: 2,
        borderRadius: 5,
        alignItems:"center"
    },
    selectedTabItem: {
        backgroundColor: "#f08554",
    },
});