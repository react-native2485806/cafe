import { useContext, useEffect, useState } from "react";
import { Alert, Button, SafeAreaView, StyleSheet, Text, TextInput, View } from "react-native";
import { Picker } from '@react-native-picker/picker';
import { db } from "@/config/db";
import UUID from 'react-native-uuid';
import DataContext from "@/app/data-context";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/config/redux";
import { addData, getData, setData, updateData } from "@/config/apis";
import { collection, doc, onSnapshot, query } from "firebase/firestore";
import { firestore } from "@/config/firebaseConfig";

export interface SaveExpenseFormComponentProps {
    navigation: any,
    route: any
}

export default function SaveExpenseFormComponent(params: SaveExpenseFormComponentProps) {

    const dispatch = useDispatch();
    const [uuid, setUUID] = useState(params.route.params?.data?.uuid ?? '');
    const [title, setName] = useState(params.route.params?.data?.title ?? '');
    const [titleBn, setNameBn] = useState(params.route.params?.data?.titleBn ?? '');
    const [amount, setAmount] = useState(params.route.params?.data?.amount?.toString() ?? '');
    const [details, setDetails] = useState(params.route.params?.data?.details?.toString() ?? '');


    const checkValidation = () => {
        return title && titleBn && amount && details
    }

    const onSubmit = async () => {
        if (!checkValidation()) {
            Alert.alert("FIll up form correctly!")
            return
        }
        // let date = new Date(new Date().getTime() - 60000 * 60 * 24 * 1);
        let date = new Date();

        if (uuid) setData("Expense", uuid, { title, titleBn, details, amount, createdAt: date })
        else addData("Expense", { title, titleBn, amount, details, createdAt: date })

        Alert.alert(
            'Success',
            uuid ? 'Updated successfully!' : 'Expense created successfully',
            [
                {
                    text: 'OK',
                    onPress: () => {
                        params.navigation.navigate("Expenses", { reload:UUID.v4() })
                    }
                }
            ],
            { cancelable: false }
        );
    }

    return (
        <SafeAreaView>
            <TextInput
                style={styles.input}
                onChangeText={setName}
                value={title}
                placeholder="English Name"
            />
            <TextInput
                style={styles.input}
                onChangeText={setNameBn}
                value={titleBn}
                placeholder="বাংলা নাম"
            />
            <TextInput
                style={styles.multiline_input}
                onChangeText={setDetails}
                value={details}
                multiline
                numberOfLines={4}
                placeholder="Details"
                editable
            />
            <TextInput
                style={styles.input}
                onChangeText={setAmount}
                value={amount}
                placeholder="Amount"
                keyboardType="numeric"
            />
            <View
                style={{ margin: 12, borderRadius: 5 }}>
                <Button
                    onPress={onSubmit}
                    title="Save"
                    color="#6ab0a3"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        borderColor: "#6ab0a3",
        color: "#3a8c7d"
    },
    multiline_input: {
        maxHeight: 100,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        borderColor: "#6ab0a3",
        color: "#3a8c7d",
    },
});
