import { useContext, useEffect, useState } from "react";
import { Alert, Button, SafeAreaView, StyleSheet, Text, TextInput, View } from "react-native";
import { Picker } from '@react-native-picker/picker';
import { db } from "@/config/db";
import UUID from 'react-native-uuid';
import DataContext from "@/app/data-context";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/config/redux";
import { updateItem } from "@/feature/catalog/redux/catalogReducer";
import { addData, getData, setData, updateData } from "@/config/apis";
import { collection, doc, onSnapshot, query } from "firebase/firestore";
import { firestore } from "@/config/firebaseConfig";

export interface SaveItemFormComponentProps {
    navigation: any,
    route: any
}

export default function SaveItemFormComponent(params: SaveItemFormComponentProps) {

    const dispatch = useDispatch();
    const [uuid, setUUID] = useState(params.route.params?.data?.uuid ?? '');
    const [name, setName] = useState(params.route.params?.data?.name ?? '');
    const [nameBn, setNameBn] = useState(params.route.params?.data?.nameBn ?? '');
    const [price, setPrice] = useState(params.route.params?.data?.price?.toString() ?? '');
    const [categoryId, setCategoryId] = useState((params.route.params?.data?.categoryId as any)?._key.path.segments[6] ?? '');
    const [category, setCategory] = useState<any[]>([])

    useEffect(() => {
        const q = query(collection(firestore, "Category"));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            const data = snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    name: i.data().name,
                    nameBn: i.data().nameBn,
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })
            setCategory(data)
        });
    }, [])

    const checkValidation = () => {
        return name && nameBn && price && categoryId && categoryId != "0"
    }

    const onSubmit = async () => {
        if (!checkValidation()) {
            Alert.alert("FIll up form correctly!")
            return
        }

        if (uuid) setData("Item", uuid, { name, nameBn, price, categoryId: doc(firestore, "/Category/" + categoryId), createdAt: new Date() })
        else addData("Item", { name, nameBn, price, categoryId: doc(firestore, "/Category/" + categoryId), createdAt: new Date() })

        Alert.alert(
            'Success',
            uuid ? 'Updated successfully!' : 'Item created successfully',
            [
                {
                    text: 'OK',
                    onPress: () => {
                        params.navigation.navigate("Discovery")
                    }
                }
            ],
            { cancelable: false }
        );
    }

    return (
        <SafeAreaView>
            <TextInput
                style={styles.input}
                onChangeText={setName}
                value={name}
                placeholder="English Name"
            />
            <TextInput
                style={styles.input}
                onChangeText={setNameBn}
                value={nameBn}
                placeholder="বাংলা নাম"
            />
            <TextInput
                style={styles.input}
                onChangeText={setPrice}
                value={price}
                placeholder="Price"
                keyboardType="numeric"
            />
            <View
                style={{ borderWidth: 1, margin: 12, borderRadius: 5, borderColor: "#6ab0a3", display: uuid ? "none" : "flex" }}>
                <Picker
                    style={{ color: "#3a8c7d" }}
                    itemStyle={{ color: "red" }}
                    selectedValue={categoryId}
                    onValueChange={(itemValue, itemIndex) =>
                        setCategoryId(itemValue)
                    }>
                    <Picker.Item label="Select Category" value={0} key={0} />
                    {
                        category.map((cat: any) => <Picker.Item label={cat.nameBn} value={cat.uuid} key={cat.uuid} />)
                    }
                </Picker>
            </View>
            <View
                style={{ margin: 12, borderRadius: 5 }}>
                <Button
                    onPress={onSubmit}
                    title="Save"
                    color="#6ab0a3"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        borderColor: "#6ab0a3",
        color: "#3a8c7d"
    },
});
