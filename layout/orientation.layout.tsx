import { ReactElement } from "react";
import { View } from "react-native";

export interface OrientationLayoutProps {
    children: ReactElement
}

export default function OrientationLayout(props: OrientationLayoutProps) {

    return (
        // <View style={{ padding: 30, backgroundColor: "yellow", flex: 1 }}>
        <View style={{ padding: 0, backgroundColor: "white", flex: 1 }}>
            {props.children}
        </View>
    )
};
