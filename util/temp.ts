
const itemCategories = [
    {
        id: 1,
        name: "Appetizers",
        nameBn: "ক্ষুধাবর্ধক খাবার",
        order: 1,
        color: "green"
    },
    {
        id: 2,
        name: "Entrees",
        nameBn: "প্রধান খাবার",
        order: 2,
        color: "black"
    },
    {
        id: 3,
        name: "Sides",
        nameBn: "পার্শ্ব খাবার",
        order: 3,
        color: "pink"
    },
    {
        id: 4,
        name: "Desserts",
        nameBn: "ডেজার্ট",
        order: 4,
        color: "blue"
    },
    {
        id: 5,
        name: "Drinks",
        nameBn: "পানীয়",
        order: 5,
        color: "red"
    },
    {
        id: 6,
        name: "Fast Food",
        nameBn: "ফাষ্ট ফুড",
        order: 6,
        color: "yellow"
    }
]


const Appetizers = [{
    id: 1,
    name: "Boiled Rice",
    nameBn: "ভাত",
    price: "50",
    order: 1
}]

const Entrees = [
    {
        id: 1,
        name: "Boiled Rice",
        nameBn: "ভাত",
        price: "50",
        order: 1
    },
    {
        id: 2,
        name: "Beef ",
        nameBn: "গরুর গোশত",
        price: "50",
        order: 2
    },
    {
        id: 3,
        name: "Cutlet",
        nameBn: "মাংসের বড়া",
        price: "50",
        order: 3
    },
    {
        id: 4,
        name: "Fowl",
        nameBn: "মুরগির মাংস",
        price: "50",
        order: 4
    },
    {
        id: 5,
        name: "Egg",
        nameBn: "ডিম",
        price: "50",
        order: 5
    },
    {
        id: 6,
        name: "Hotch Potch",
        nameBn: "কিচুড়ি",
        price: "50",
        order: 6
    },
    {
        id: 7,
        name: "Fish",
        nameBn: "মাছ",
        price: "50",
        order: 7
    },
    {
        id: 8,
        name: "Mutton",
        nameBn: "খাসির মাংস",
        price: "50",
        order: 8
    },
    {
        id: 9,
        name: "Pulse",
        nameBn: "ডাল",
        price: "50",
        order: 9
    },
    {
        id: 10,
        name: "Pillau",
        nameBn: "পোলাও",
        price: "50",
        order: 10
    },

    {
        id: 11,
        name: "Pillau",
        nameBn: "পোলাও",
        price: "50",
        order: 11
    },

    {
        id: 12,
        name: "Pillau",
        nameBn: "পোলাও",
        price: "50",
        order: 12
    },
]

const Sides = [{
    id: 1,
    name: "Boiled Rice",
    nameBn: "ভাত",
    price: "50",
    order: 1
}]



const Desserts = [{
    id: 1,
    name: "Boiled Rice",
    nameBn: "ভাত",
    price: "50",
    order: 1
}]


const FastFood = [{
    id: 1,
    name: "Boiled Rice",
    nameBn: "ভাত",
    price: "50",
    order: 1
}]


const Drinks = [{
    id: 1,
    name: "Boiled Rice",
    nameBn: "ভাত",
    price: "50",
    order: 1
}]

const categoryItems = [

    Appetizers, Entrees, Sides, Desserts, FastFood, Drinks
]

interface TokenListProps {
    id: number;
    token: number;
    total: number;
    createdAt: string;
}

const tokenList: TokenListProps[] = [
    {
        id: 1,
        token: 20240216001,
        total: 240,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 2,
        token: 20240216002,
        total: 440,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 3,
        token: 20240216003,
        total: 670,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 4,
        token: 20240216004,
        total: 2420,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 5,
        token: 20240216005,
        total: 6040,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 6,
        token: 20240216006,
        total: 590,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 7,
        token: 20240216007,
        total: 870,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 8,
        token: 20240216008,
        total: 985,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 9,
        token: 20240216009,
        total: 874,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 10,
        token: 20240216010,
        total: 376,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 11,
        token: 20240216011,
        total: 2040,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
    {
        id: 12,
        token: 20240216012,
        total: 256,
        createdAt: "Fri Feb 16 2024 19:17:07 GMT+0600"
    },
]

const itemSales = [
    {
        id: 1,
        name: "Chicken",
        nameBn: "মুরগি",
        quantity: 64,
        price: 60,
        totalPrice: 753,
    },
    {
        id: 2,
        name: "Beaf",
        nameBn: "গরু",
        quantity: 99,
        price: 60,
        totalPrice: 363,
    },
    {
        id: 3,
        name: "Telapiya Mach",
        nameBn: "তেলাপিয়া মাছ",
        quantity: 79,
        price: 60,
        totalPrice: 632,
    },
    {
        id: 4,
        name: "Mutton",
        nameBn: "খাসি",
        quantity: 19,
        price: 60,
        totalPrice: 395,
    },
    {
        id: 5,
        name: "Rice",
        nameBn: "ভাত",
        quantity: 149,
        price: 60,
        totalPrice: 538,
    },
    {
        id: 6,
        name: "Telapiya Mach",
        nameBn: "তেলাপিয়া মাছ",
        quantity: 66,
        price: 60,
        totalPrice: 986,
    },
    {
        id: 7,
        name: "Telapiya Mach",
        nameBn: "তেলাপিয়া মাছ",
        quantity: 21,
        price: 60,
        totalPrice: 291,
    },
    {
        id: 8,
        name: "Telapiya Mach",
        nameBn: "তেলাপিয়া মাছ",
        quantity: 42,
        price: 60,
        totalPrice: 894,
    },
    {
        id: 9,
        name: "Telapiya Mach",
        nameBn: "তেলাপিয়া মাছ",
        quantity: 29,
        price: 60,
        totalPrice: 294,
    },
    {
        id: 10,
        name: "Telapiya Mach",
        nameBn: "তেলাপিয়া মাছ",
        quantity: 44,
        price: 60,
        totalPrice: 240,
    },
    {
        id: 11,
        name: "Telapiya Mach",
        nameBn: "তেলাপিয়া মাছ",
        quantity: 39,
        price: 60,
        totalPrice: 940,
    },
    {
        id: 12,
        name: "Telapiya Mach",
        nameBn: "তেলাপিয়া মাছ",
        quantity: 49,
        price: 60,
        totalPrice: 2940,
    }
]

const data = [
    { key: 'Devin' },
    { key: 'Dan' },
    { key: 'Dominic' },
    { key: 'Jackson' },
    { key: 'James' },
    { key: 'Joel' },
    { key: 'John' },
    { key: 'Jillian' },
    { key: 'Jimmy' },
    { key: 'Julie' },
    { key: 'Defhvin' },
    { key: 'Dahfn' },
    { key: 'Domfhinic' },
    { key: 'Jachfkson' },
    { key: 'Jamdges' },
    { key: 'Jogdgel' },
    { key: 'Jodgdghn' },
    { key: 'Jildglian' },
    { key: 'Jimdgmy' },
    { key: 'Julidge' },
]

export {
    itemCategories,
    data,
    categoryItems,
    tokenList,
    itemSales,
    Entrees
}