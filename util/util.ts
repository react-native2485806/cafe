
type MyObjectType = {
    [key: string]: any;
};

const objectKeyTypeNumberToString = (data: any[], key: string): any => {
    return data.map((item) => {
        item[key] = item[key].toString()
        return item
    })
}

interface KeyReplaceProps {
    key: string,
    newKey: string
}

const addObjectKeyFrom = (data: MyObjectType[], keyReplace: KeyReplaceProps[]): any => {
    return data.map((item) => {
        for (let keys of keyReplace) {
            item[keys.newKey] = item[keys.key]
        }
        return item
    })
}



const arrayOfDateOfMonthTillToday = () => {
    let today = new Date()
    let date = today.getDate()
    let dates = []
    for (let i = 1; i <= date; i++) {
        let d = new Date()
        let nd = d.setDate(i)
        let dd = new Date(nd)
        dates.push(dd.toLocaleDateString("en-CA"))
    }
    return dates
}

function formatAMPM(date: any) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}



export {
    objectKeyTypeNumberToString,
    addObjectKeyFrom,
    arrayOfDateOfMonthTillToday,
    formatAMPM
}