import { DefaultFooter } from "@/components/footer";
import { DefaultHeader } from "@/components/header";
import { Slot } from "expo-router";
import { View } from "react-native";

export default function MenuLayout() {

    return (
        <View style={{ flex: 1, bottom:-20 }}>
            <DefaultHeader />
            <Slot />
            <DefaultFooter />
        </View>
    )
}