import { DefaultHeader } from "@/components/header";
import { LoginView } from "@/view/auth";
import { DashboardView } from "@/view/dashboard";
import { useEffect, useState } from "react";
import { Alert, View } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import { createAllTable, db, dropAllTable } from "@/config/db";
import { syncFirebaseDbToSqlite, syncSqliteToFirebaseDb } from "@/config/api-db";
import { useDispatch } from "react-redux";
import { updateCategory, updateItem, updateItemSale, updateToken } from "@/feature/catalog/redux/catalogReducer";

export default function main() {

    const [authorized, setAuthorized] = useState(false)
    const [pass, setPass] = useState('')
    const [passLength, setPassLength] = useState(6)
    const [warning, setWarning] = useState(false)
    const [passwords, setPasswords] = useState(['123465'])
    const [saveMode, setSaveMode] = useState<boolean>(false)
    const [online, setOnline] = useState<boolean>()

    const onKeyPress = (num: string) => {
        num !== "X" && num !== ">" && pass.length < passLength && setPass((p) => p + num)
        if (pass == "101101") {
            setPass('')

            if (num === "1") {
            }
            else if (num === "2") {
            }
            else if (num === "3") {
            }
            else if (num === "4") {
                setSaveMode(true)
            }
            else if (num === "5") {
            }
            else if (num === "6") {
            }
            else if (num === "7") {
            }
            else if (num === "9") {
            }
            else if (num === "0") {
            }
        }

        if (num === "X") {
            setPass('');
            setWarning(false)
        }
        num === ">" && checkPass()
    }

    useEffect(() => {
        saveMode && alert("`Add New Password` Mode Enabled!")
    }, [saveMode])

    const checkPass = () => {
        if (saveMode) {
            setPasswords(p => p.concat(pass))
            setPass('')
            setSaveMode(false)
        } else {
            if (online != true) {
                alert("Check internet connection first!")
                return
            }
            if (passwords.includes(pass)) {
                setAuthorized(true)
                setPass('')
            } else {
                setWarning(true)
            }
        }
    }

    useEffect(() => {
        NetInfo.addEventListener((state) => {
            console.log(state)
            setOnline(state.isConnected ?? false);
        });
    })

    useEffect(() => {
        if (online == false) alert("Check internet connection first!")
    }, [online])


    const onPress = (userName: string, password: string) => {
        if (userName == "user1919" && password == "1234") {
            setAuthorized(true)
        }
        else alert("Mismatch credentials!")
    }


    return (
        <>
            <View style={{ backgroundColor: "#f7f7f7", flex: 1, display: authorized ? "flex" : "none" }}>
                <DashboardView />
            </View>
            <View style={{ display: authorized ? "none" : "flex", justifyContent: "center", alignContent: "space-between", flex: 1, }}>
                <DefaultHeader />
                <LoginView onPress={onPress} pass={pass} onKeyPress={onKeyPress} warning={warning} setSaveMode={setSaveMode} saveMode={saveMode} />
            </View>
        </>
    )
}
