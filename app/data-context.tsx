// MyContext.js
import { deleteData, deleteDataByRef } from '@/config/apis';
import { db } from '@/config/db';
import { ItemSale } from '@/config/type';
import React, { FC, createContext, useEffect, useState } from 'react';
import { Alert } from 'react-native';
import UUID from 'react-native-uuid';
import ThermalPrinterModule from 'react-native-thermal-printer';
import { useSelector } from 'react-redux';
import { RootState } from '@/config/redux';

const DataContext = createContext<any>({});

export const DataProvider = ({ children }: { children: JSX.Element }) => {

    const [itemCategories, setItemCategories] = useState<any[]>([])
    const [items, setItems] = useState<any[]>([])
    const [loading, setLoading] = useState<boolean>(true)
    const [totalAmount, setTotalAmount] = useState<number>(0)
    const [tokens, setTokens] = useState<any[]>([])
    const [itemSale, setItemSale] = useState<any[]>([])
    const [itemSales, setItemSales] = useState<any[]>([])
    const [selectedItem, setSelectedItem] = useState<any[]>([])
    const [orderId, setOrderId] = useState<string>('')
    const [totalSale, setTotalSale] = useState(0)
    const [itemQuantity, setItemQuantity] = useState<any[]>([]);
    const catalogReducer = useSelector((state: RootState) => state.catalogReducer);



    const deleteToken = (uuid: string) => {
        db.transaction((tx) => {
            tx.executeSql(`DELETE from Token WHERE Token.uuid="${uuid}"`, [],
                (tx, res) => {
                    fetchTokens()
                    deleteData("Token", uuid)
                    tx.executeSql(`SELECT * from ItemSale WHERE ItemSale.tokenId="${uuid}"`, [],
                        (tx, res) => {
                            res?.rows?._array?.forEach((sale: ItemSale) => {
                                tx.executeSql(`DELETE from ItemSale WHERE ItemSale.tokenId="${uuid}"`)
                                deleteData("ItemSale", sale.uuid)
                            })
                            fetchItemSales()
                        },
                        (tx, error): any => false
                    );
                },
                (tx, error): any => false
            );
        });
    }

    const fetchTokens = () => {
        db.transaction((tx) => {
            tx.executeSql(`SELECT * from Token ORDER BY createdAt DESC`, [],
                (tx, res) => {
                    setTokens(res.rows._array)
                },
                (tx, error): any => false
            );
        });
    }

    const getToadyTotalSale = () => {
        db.transaction((tx) => {
            tx.executeSql(`SELECT * from ItemSale ORDER BY createdAt ASC`, [],
                (tx, res) => {
                    let salesOfToday = res.rows._array.filter((i) => new Date(i.createdAt).getTime() > (new Date().setHours(0) - (60000 * 60 * 6)))
                    let total = 0
                    for (let i of salesOfToday) {
                        total = total + i.quantity * i.price
                    }
                    setTotalSale(total)
                },
                (tx, error): any => false
            );
        });
    }

    const fetchItemSales = () => {
        db.transaction((tx) => {
            tx.executeSql(`SELECT * from ItemSale ORDER BY createdAt ASC`, [],
                (tx, res) => {
                    let salesOfToday = res.rows._array.filter((i) => new Date(i.createdAt).getTime() > (new Date().setHours(0) - (60000 * 60 * 6)))
                    let uuids = salesOfToday.map((i) => i.itemId)
                    let uniqueUUIDs = Array.from(new Set(uuids))
                    let quantitySalesByItem = uniqueUUIDs.map((itemId) => {
                        let quantity = 0;
                        let itemNameEn = '';
                        let itemNameBn = '';
                        let price = 0;
                        for (let item of salesOfToday.filter((i) => i.itemId == itemId)) {
                            quantity = quantity + item.quantity
                            itemNameEn = item.itemNameEn
                            itemNameBn = item.itemNameBn
                            price = item.price
                        }
                        return { itemId, quantity, itemNameEn, itemNameBn, price }
                    })
                    setItemSales(quantitySalesByItem)
                },
                (tx, error): any => false
            );
        });
    }

    const fetchItemSaleByToken = (uuid: string) => {
        setLoading(true)
        db.transaction((tx) => {
            tx.executeSql(`SELECT * from ItemSale WHERE tokenId="${uuid}"`, [],
                // tx.executeSql(`SELECT * from ItemSale WHERE tokenId="${uuid}" WHERE createdAt="${new Date()}" ORDER BY createdAt DESC `, [],
                (tx, res) => {
                    setItemSale(res.rows._array)
                    setLoading(false)
                },
                (tx, error): any => false
            );
        });
    }

    useEffect(() => {
        fetchTokens()
    }, []);


    const generatePrintPaper = (items: any[], uuid: string) => {
        let total = 0
        for (let i of items) {
            total = total + (parseInt(i.price) * parseInt(i.quantity))
        }
        let textHead = "[L]<font size='big'><b>   Vista Cafe</b></font>\n"
            + "[L]      NSTU, Shanti Niketon\n"
            + "   " + new Date().toDateString() + " " + new Date().toLocaleTimeString() + "\n"
            + "[L]Order # " + uuid.substring(uuid.length - 12, uuid.length) + "\n"
            + "[L]Qty    Item[C]Price\n"
            + "--------------------------------"
        let textTail = "\n--------------------------------\n[L]<b>Total</b>[C]" + total.toFixed(2)
        let text = textHead
        for (let i of items) {
            console.log(i)
            text = text + "\n" + "[L]" + i.quantity + " X    " + i.name + " (" + i.price + ")[C]" + (parseInt(i.price) * parseInt(i.quantity)).toFixed(2)
        }
        return text + textTail
    }

    const print = async (items: any[], uuid: string, viewRef: React.MutableRefObject<any>, viewItemRef: React.MutableRefObject<any>, viewItemRef2: React.MutableRefObject<any>, viewItemSummaryRef: React.MutableRefObject<any>) => {
        const paper = generatePrintPaper(items, uuid)
        viewRef.current.capture().then(async (uri: any) => {
            console.log("do something with ", uri);
            viewItemRef.current.capture().then(async (urii: any) => {
                console.log("do something with ", urii);
                viewItemRef2.current.capture().then(async (urii2: any) => {
                    console.log("do something with ", urii2);
                    viewItemSummaryRef.current.capture().then(async (uris: any) => {
                        console.log("do something with ", uris);

                        let payload = '<img>' + uri + '</img>\n<img>' + urii + '</img>\n<img>' + uris + '</img>'
                        // let payload = '<img>' + uris + '</img>'
                        if (items.length > 7) payload = '<img>' + uri + '</img>\n<img>' + urii + '</img>\n<img>' + urii2 + '</img>\n<img>' + uris + '</img>'

                        try {
                            await ThermalPrinterModule.printBluetooth({
                                payload: payload,
                                printerNbrCharactersPerLine: 3,
                            });
                        } catch (err: any) {
                            //error handling
                            deleteTokenAndItemSales(uuid).then(() => {
                                fetchTokens()
                                fetchItemSales()
                            })
                            Alert.alert("Failed!", err.message)
                            console.log("--",err);
                        }
                    })
                })
            })

        });
        console.log("call me")
        resetQuantity()
        fetchTokens()
        fetchItemSales()
    }

    const deleteTokenAndItemSales = async (uuid: string) => {
        db.transaction(async (tx) => {
            tx.executeSql(`DELETE FROM Token WHERE uuid="${uuid}"`)
            tx.executeSql(`DELETE FROM ItemSale WHERE tokenId="${uuid}"`)
        })
    }

    const generateItemSale = async (items: any[], viewRef: React.MutableRefObject<any>, viewItemRef: React.MutableRefObject<any>, viewItemRef2: React.MutableRefObject<any>, viewItemSummaryRef: React.MutableRefObject<any>) => {
        let uuid = UUID.v4()
        console.log(items)
        setOrderId(uuid.toString())
        db.transaction((tx) => {
            tx.executeSql(`INSERT INTO Token (uuid, amount, createdAt) VALUES ('${uuid}', ${totalAmount}, '${new Date()}')`, [],
                async (tx, res) => {
                    try {
                        for (let item of items ?? []) {
                            tx.executeSql(`INSERT INTO ItemSale (uuid, itemId, tokenId, quantity, price, itemNameBn, itemNameEn, createdAt) VALUES ('${UUID.v4()}', '${item.uuid}', '${uuid}', ${item.quantity}, ${item.price}, '${item.nameBn}','${item.name}', '${new Date()}'  )`, [],
                                (tx, res) => { },
                                (tx, error) => {
                                    tx.executeSql(`DELETE FROM Token WHERE uuid="${uuid}"`)
                                    tx.executeSql(`DELETE FROM ItemSale WHERE tokenId="${uuid}"`)
                                    alert("Something went wrong on generating sale items!")
                                    return false
                                }
                            )
                        }
                        await print(items, uuid.toString(), viewRef, viewItemRef, viewItemRef2, viewItemSummaryRef)
                    }
                    catch (e) {
                        console.log(e)
                        tx.executeSql(`DELETE FROM Token WHERE uuid="${uuid}"`)
                        tx.executeSql(`DELETE FROM ItemSale WHERE tokenId="${uuid}"`)
                        Alert.alert(
                            'Failed',
                            'Generated token and selected items has been removed!',
                            [
                                {
                                    text: 'OK',
                                    onPress: () => alert('Printing bill paper')
                                }
                            ],
                            { cancelable: false }
                        );
                    }

                },
                (tx, error) => {
                    alert("Something went wrong on token generation")
                    return false
                }
            )
        })
    }
    const getSelectedItems = () => {
        
    }//itemQuantity.map((i) => ({ ...items.filter((j) => j.uuid == i.uuid)[0], quantity: i.quantity }))

    useEffect(() => {
        let amount = 0
        for (let item of getSelectedItems() ?? []) {
            amount = amount + item.price * item.quantity
        }
        setTotalAmount(amount)
    }, [itemQuantity])


    const onClickPrint = async (viewRef: React.MutableRefObject<any>, viewItemRef: React.MutableRefObject<any>, viewItemRef2: React.MutableRefObject<any>, viewItemSummaryRef: React.MutableRefObject<any>) => {
        const items = getSelectedItems()
        setSelectedItem(items)
        if (!catalogReducer.selectedMenu?.length) {
            Alert.alert("Sorry!", "No items is selected!")
        } else {
            await generateItemSale(items, viewRef, viewItemRef, viewItemRef2, viewItemSummaryRef)
        }
    }

    const resetItemSelection = () => {
        setItemQuantity([])

    }


    return (
        <DataContext.Provider value={{ setItemQuantity, orderId, selectedItem, getSelectedItems, itemQuantity, loading, items, itemCategories, resetItemSelection, onClickPrint, totalAmount, tokens, fetchItemSaleByToken, itemSale, fetchItemSales, itemSales, totalSale, getToadyTotalSale, deleteToken, fetchTokens }}>
            {children}
        </DataContext.Provider>
    );
};

export default DataContext;
