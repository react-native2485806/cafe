import { Text } from "@rneui/base";
import { useContext, useState } from "react";
import { StyleSheet, TouchableWithoutFeedback, View } from "react-native";
import { FontAwesome } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import DataContext from "@/app/data-context";

export interface DiscoveryFeatureProps {
    navigation: any
}

export default function DiscoveryFeature({ navigation }: DiscoveryFeatureProps) {

    const [color, setColor] = useState('white')
    const [selectedIndex, setSelectedIndex] = useState(0)
    const baseColor = "#ff7b00"
    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <TouchableWithoutFeedback
                    onPressIn={() => { setColor(baseColor), setSelectedIndex(1) }}
                    onPressOut={() => { setColor('white'), setSelectedIndex(0) }}
                    onPress={() => { navigation.navigate('Token History', { name: 'Token History' }) }}
                >
                    <View style={{ padding: 20, height: 200, width: '40%', margin: 20, backgroundColor: selectedIndex == 1 ? color : 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <FontAwesome name="history" style={{}} size={60} color={selectedIndex == 1 ? 'white' : baseColor} />
                        <Text style={{ marginTop: 10, color: selectedIndex == 1 ? 'white' : baseColor }}>
                            Token History
                        </Text>
                        {/* <Text style={{ marginTop: 2, color: selectedIndex == 1 ? 'white' : baseColor }}>
                            Total 334
                        </Text> */}
                    </View>
                </TouchableWithoutFeedback >
                <TouchableWithoutFeedback
                    onPressIn={() => { setColor(baseColor), setSelectedIndex(2) }}
                    onPressOut={() => { setColor('white'), setSelectedIndex(0) }}
                    onPress={() => { navigation.navigate('Item Sales', { name: 'Item Sales' }) }}
                >
                    <View style={{ padding: 20, height: 200, width: '40%', margin: 20, backgroundColor: selectedIndex == 2 ? color : 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <FontAwesome5 name="sellsy" style={{}} size={60} color={selectedIndex == 2 ? 'white' : baseColor} />
                        <Text style={{ marginTop: 10, color: selectedIndex == 2 ? 'white' : baseColor }}>
                            Item Sell
                        </Text>
                        {/* <Text style={{ marginTop: 2, color: selectedIndex == 2 ? 'white' : baseColor }}>
                            Total 327
                        </Text> */}
                    </View>
                </TouchableWithoutFeedback >
            </View>

            <View style={{ flexDirection: 'row' }}>
                <TouchableWithoutFeedback
                    onPressIn={() => { setColor(baseColor), setSelectedIndex(3) }}
                    onPressOut={() => { setColor('white'), setSelectedIndex(0) }}
                    onPress={() => { navigation.navigate('Cash', { name: 'Cash' }) }}
                >
                    <View style={{ padding: 20, height: 200, width: '40%', margin: 20, backgroundColor: selectedIndex == 3 ? color : 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <MaterialCommunityIcons name="cash" size={60} color={selectedIndex == 3 ? 'white' : baseColor} />
                        <Text style={{ marginTop: 10, color: selectedIndex == 3 ? 'white' : baseColor }}>
                            Cash
                        </Text>
                        {/* <Text style={{ marginTop: 2, color: selectedIndex == 3 ? 'white' : baseColor }}>
                            Total 3920
                        </Text> */}
                    </View>
                </TouchableWithoutFeedback >

                <TouchableWithoutFeedback
                    onPressIn={() => { setColor(baseColor), setSelectedIndex(6) }}
                    onPressOut={() => { setColor('white'), setSelectedIndex(0) }}
                    onPress={() => {navigation.navigate('Expense', { name: 'Expense' }) }}
                >
                    <View style={{ padding: 20, height: 200, width: '40%', margin: 20, backgroundColor: selectedIndex == 6 ? color : 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <AntDesign name="export" style={{}} size={60} color={selectedIndex == 6 ? 'white' : baseColor} />
                        <Text style={{ marginTop: 10, color: selectedIndex == 6 ? 'white' : baseColor }}>
                            Expense
                        </Text>
                        {/* <Text style={{ marginTop: 2, color: selectedIndex == 1 ? 'white' : baseColor }}>
                            Total 334
                        </Text> */}
                    </View>
                </TouchableWithoutFeedback>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                {/* <TouchableWithoutFeedback
                    onPressIn={() => { setColor(baseColor), setSelectedIndex(7) }}
                    onPressOut={() => { setColor('white'), setSelectedIndex(0) }}
                    onPress={() => navigation.navigate('All Items', { name: 'All Items' })}
                >
                    <View style={{ padding: 20, height: 200, width: '40%', margin: 20, backgroundColor: selectedIndex == 7 ? color : 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <MaterialIcons name="category" size={60} color={selectedIndex == 7 ? 'white' : baseColor} />
                        <Text style={{ marginTop: 10, color: selectedIndex == 7 ? 'white' : baseColor }}>
                            All Category
                        </Text>
                    </View>
                </TouchableWithoutFeedback > */}
                <TouchableWithoutFeedback
                    onPressIn={() => { setColor(baseColor), setSelectedIndex(4) }}
                    onPressOut={() => { setColor('white'), setSelectedIndex(0) }}
                    onPress={() => navigation.navigate('All Items', { name: 'All Items' })}
                >
                    <View style={{ padding: 20, height: 200, width: '40%', margin: 20, backgroundColor: selectedIndex == 4 ? color : 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <MaterialIcons name="menu-book" size={60} color={selectedIndex == 4 ? 'white' : baseColor} />
                        <Text style={{ marginTop: 10, color: selectedIndex == 4 ? 'white' : baseColor }}>
                            All Items
                        </Text>
                        {/* <Text style={{ marginTop: 2, color: selectedIndex == 4 ? 'white' : baseColor }}>
                            Total 39
                        </Text> */}
                    </View>
                </TouchableWithoutFeedback >
            </View>
            <View style={{ flexDirection: 'row' }}>
                {/* <TouchableWithoutFeedback
                    onPressIn={() => { setColor(baseColor), setSelectedIndex(5) }}
                    onPressOut={() => { setColor('white'), setSelectedIndex(0) }}
                    onPress={() => navigation.navigate('Daily Sheet', { name: 'Daily Sheet' })}
                >
                    <View style={{ padding: 20, height: 200, width: '40%', margin: 20, backgroundColor: selectedIndex == 5 ? color : 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <MaterialCommunityIcons name="clipboard-file-outline" size={60} color={selectedIndex == 5 ? 'white' : baseColor} />
                        <Text style={{ marginTop: 10, color: selectedIndex == 5 ? 'white' : baseColor }}>
                            Daily Sheet
                        </Text>
                    </View>
                </TouchableWithoutFeedback > */}
                {/* <TouchableWithoutFeedback
                    onPressIn={() => { setColor(baseColor), setSelectedIndex(5) }}
                    onPressOut={() => { setColor('white'), setSelectedIndex(0) }}
                    onPress={() => navigation.navigate('Files', { name: 'Files' })}
                >
                    <View style={{ padding: 20, height: 200, width: '40%', margin: 20, backgroundColor: selectedIndex == 5 ? color : 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <MaterialCommunityIcons name="clipboard-file-outline" size={60} color={selectedIndex == 5 ? 'white' : baseColor} />
                        <Text style={{ marginTop: 10, color: selectedIndex == 5 ? 'white' : baseColor }}>
                            Files
                        </Text>
                        <Text style={{ marginTop: 2, color: selectedIndex == 5 ? 'white' : baseColor }}>
                            Total 92
                        </Text>
                    </View>
                </TouchableWithoutFeedback > */}
            </View>
        </View >
    )
};

const styles = StyleSheet.create({
    heading: {
        fontSize: 18,
        fontWeight: '600',
        marginBottom: 13,
    },
    card: {
        backgroundColor: 'white',
        borderRadius: 8,
        paddingVertical: 45,
        paddingHorizontal: 25,
        width: '100%',
        marginVertical: 10,
    },
    shadowProp: {
        shadowColor: '#171717',
        shadowOffset: { width: -2, height: 4 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
    },
});