import { useEffect, useState } from "react";
import { Text } from "@rneui/base";
import { router } from "expo-router";
import { StyleSheet, TextInput, TouchableOpacity, View } from "react-native";

export interface LoginFeatureProps {

    onPress: (userName: string, password: string) => void;
    onKeyPress: (num: string) => void;
    pass: string;
    warning: boolean;
    setSaveMode: (mode: boolean) => void
    saveMode:boolean
}

export default function LoginFeature(props: LoginFeatureProps) {

    const { onKeyPress, pass, warning, setSaveMode, saveMode } = props

    const [userName, setUserName] = useState("")
    const [password, setPassword] = useState("")
    const [star, setStart] = useState("")

    useEffect(() => {
        let st = ''
        for (let i = 0; i < pass.length; i++) {
            st = st + '*'
        }
        setStart(st)
    }, [pass])

    const onPress = () => {
        props.onPress(userName, password)
    }

    const textBackground = "#62869e"
    const cancelBackground = "#fa6132"
    const submitBackground = "#2abf84"
    const layoutBackground = "#f7f7f7"
    const borderBackground = "#b5b5b5"
    const warningBackground = "#ff6038"
    const saveModeBackground = "#ff1900"
    const fontColor = "#fff"

    return (
        <View style={{flex:1, justifyContent:"center", backgroundColor: saveMode? saveModeBackground:"#fff"}}>
            <View style={{ display: 'flex', backgroundColor: layoutBackground, margin: 20, padding: 10, borderRadius: 10, borderWidth: 1, borderColor: warning ? warningBackground : borderBackground }}>
                <Text style={{ textAlign: "center", fontSize: 30 }}>{star}</Text>
            </View>
            <View style={{ display: 'flex', flexDirection: "row", flexWrap: "wrap", backgroundColor:layoutBackground, margin: 20, padding: 10, borderRadius: 10 }}>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("1")}>
                    <Text style={{ fontSize: 22, backgroundColor: textBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        1
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("2")}>
                    <Text style={{ fontSize: 22, backgroundColor: textBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        2
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("3")}>
                    <Text style={{ fontSize: 22, backgroundColor: textBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        3
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("4")}>
                    <Text style={{ fontSize: 22, backgroundColor: textBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        4
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("5")}>
                    <Text style={{ fontSize: 22, backgroundColor: textBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        5
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("6")}>
                    <Text style={{ fontSize: 22, backgroundColor: textBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        6
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("7")}>
                    <Text style={{ fontSize: 22, backgroundColor: textBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        7
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("8")}>
                    <Text style={{ fontSize: 22, backgroundColor: textBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        8
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("9")}>
                    <Text style={{ fontSize: 22, backgroundColor: textBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        9
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("X")}>
                    <Text style={{ fontSize: 22, fontWeight: "700", backgroundColor: cancelBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        X
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress("0")}>
                    <Text style={{ fontSize: 22, backgroundColor: textBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        0
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ width: "33.33%", height: 70, alignItems: "center", marginVertical: 10 }} onPress={() => onKeyPress(">")}>
                    <Text style={{ fontSize: 22, fontWeight: "700", backgroundColor: submitBackground, paddingVertical: 20, paddingHorizontal: 35, color: fontColor, borderRadius: 5 }}>
                        {'>'}
                    </Text>
                </TouchableOpacity>
                {/* <Text style={{ width: "33.33%" }}>2</Text>
                <Text style={{ width: "33.33%" }}>3</Text>
                <Text style={{ width: "33.33%" }}>4</Text>
                <Text style={{ width: "33.33%" }}>5</Text>
                <Text style={{ width: "33.33%" }}>6</Text>
                <Text style={{ width: "33.33%" }}>7</Text>
                <Text style={{ width: "33.33%" }}>8</Text>
                <Text style={{ width: "33.33%" }}>9</Text>
                <Text style={{ width: "33.33%" }}>X</Text>
                <Text style={{ width: "33.33%" }}>0</Text>
                <Text style={{ width: "33.33%" }}>{'>'}</Text> */}
            </View>

            {/* <TextInput
                style={styles.input}
                onChangeText={setUserName}
                value={userName}
                placeholder="Username"
            />
            <TextInput
                style={styles.input}
                onChangeText={setPassword}
                value={password}
                placeholder="Password"
                // keyboardType="numeric"
                textContentType="password"
                secureTextEntry={true}
            /> */}
            {/* <TouchableOpacity style={styles.button} onPress={() => Alert.alert('Simple TO pressed')}> */}
            {/* <TouchableOpacity style={styles.button} onPress={onPress}>
                <Text style={{ color: "white" }}>SUBMIT</Text>
            </TouchableOpacity> */}
        </View>
    )

};

const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 44 / 5
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#62869e',
        padding: 10,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 44 / 5,
    },
});