import { db } from "@/config/db"
import { firestore } from "@/config/firebaseConfig"
import { Expense } from "@/config/type"
import { collection, onSnapshot, orderBy, query, where } from "firebase/firestore"
import { useEffect, useState } from "react"
import { Alert } from "react-native"

export interface ExpenseHookProps {
    navigation: any
    route: any
}

export interface ExpenseHookInterface {
    revert: number,
    setRevert: (revert: number) => any,
    loading: boolean,
    total: number,
    todayFirst: Date,
    dayMs: number,
    date: Date
    setDate: (date: Date) => void
    expenses: Expense[]
}

export const useExpenseHook = (props: ExpenseHookProps): ExpenseHookInterface => {


    const { navigation, route } = props
    const [revert, setRevert] = useState(route?.params?.revert || 0)
    const [loading, setLoading] = useState(false)
    const [total, setTotal] = useState(0)
    const [dayMs, setDayMs] = useState(60000 * 60 * 24)
    const [todayFirst, setTodayFirst] = useState(new Date(new Date(new Date(new Date().setHours(0)).setMinutes(0)).setSeconds(0)))
    const [date, setDate] = useState<Date>(new Date())
    const [expenses, setExpenses] = useState<Expense[]>([])

    useEffect(() => {
        route?.params?.reload && setRevert(0)
    }, [route?.params?.reload])

    useEffect(() => {
        route?.params?.revert && setRevert(route?.params?.revert)
    }, [route?.params?.revert])

    useEffect(() => {
        // if (Math.abs(revert) > 1) {
        //     Alert.alert("Need to purchase", "One day permission only!")
        //     setRevert(0)
        //     return;
        // }
        setLoading(true)
        getExpense()
        setDate(new Date(new Date().getTime() - dayMs * Math.abs(revert)))
    }, [revert, route?.params?.revert])

    const getExpense = () => {
        setLoading(true)
        const startDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + revert);
        const endDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1 + revert);

        const q = query(collection(firestore, "Expense"), where("createdAt", ">=", startDate), where("createdAt", "<", endDate), orderBy("createdAt", "desc"));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            const exp: Expense[] = snapshot.docs.map((i: any) => {
                return {
                    uuid: i.id,
                    ...i.data(),
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })
            setLoading(false)
            setExpenses(exp)
        });
    }

    return {
        revert,
        setRevert,
        loading,
        total,
        todayFirst,
        dayMs,
        date,
        setDate,
        expenses
    }
}