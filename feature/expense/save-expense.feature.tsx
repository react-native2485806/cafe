import { KeyboardAvoidingView, ScrollView, View } from "react-native";
import { useExpenseHook } from "./hooks/useExpense.hook";
import { SaveExpenseFormComponent } from "@/components/form";
import { AntDesign } from "@expo/vector-icons";

export interface SaveExpenseFeatureProps {
    navigation: any,
    route: any,
}

export default function SaveExpenseFeature(props: SaveExpenseFeatureProps) {

    return (
        <>
            <ScrollView style={{ display: "flex", flex: 1, backgroundColor: "white", margin: 10, paddingVertical: 30, borderRadius: 5 }}>
                <KeyboardAvoidingView behavior='height'>
                    <View style={{ paddingBottom: 30 }}>
                        <AntDesign name="export" size={140} color="#6ab0a3" style={{ display: "flex", alignSelf: "center" }} />
                        <SaveExpenseFormComponent {...props} />
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        </>
    )
};
