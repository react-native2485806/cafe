
import { Text } from "@rneui/base";
import { useContext, useState } from "react";
import { Alert, Button, ScrollView, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, View } from "react-native";
import { FontAwesome } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import DataContext from "@/app/data-context";
import { db } from "@/config/db";
import * as Network from 'expo-network';
import { AntDesign } from '@expo/vector-icons';

export interface DailySheetFeatureProps {

}

export default function DailySheetFeature(params: DailySheetFeatureProps) {

    const { } = useContext(DataContext);


    return (
        <>
            <View style={{ display: "flex", flexDirection: "row", maxHeight: 73, borderRadius: 5, backgroundColor: "white", padding: 5, marginTop: 5, margin: 10, justifyContent: "space-between" }}>
                <TouchableOpacity
                    onPress={() => { console.log("left") }}
                    style={{ padding: 12, backgroundColor: "#ffb375", borderRadius: 5 }}
                >
                    <AntDesign name="left" size={24} color="white" />
                </TouchableOpacity>
                <Text style={{fontSize:17, color:"#ffb375", alignSelf:"center", fontWeight:"800"}}>2024- 02 - 20</Text>
                <TouchableOpacity
                    onPress={() => { console.log("left") }}
                    style={{ padding: 12, backgroundColor: "#ffb375", borderRadius: 5 }}
                >
                    <AntDesign name="right" size={24} color="white" />
                </TouchableOpacity>
            </View>

        </>
    )

};

const styles = StyleSheet.create({
});
