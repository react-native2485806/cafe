import { Alert, Text, TouchableOpacity } from "react-native";
import { TokenHistoryListComponent } from "@/components/item-list";
import { useEffect, useState } from "react";
import { View } from "react-native";
import { collection, onSnapshot, orderBy, query, where } from "firebase/firestore";
import { firestore } from "@/config/firebaseConfig";
import { AntDesign } from "@expo/vector-icons";
import DatePicker from 'react-native-date-picker'
import { ActivityIndicator } from "react-native";

export interface TokenHistoryFeatureProps {
    navigation: any
    route: any
}


export default function TokenHistoryFeature(props: TokenHistoryFeatureProps) {

    const [tokens, setTokens] = useState<any[]>([])
    const [date, setDate] = useState<Date>(new Date())
    const [loading, setLoading] = useState(false)
    const [revert, setRevert] = useState(0)
    const [todayFirst, setTodayFirst] = useState(new Date(new Date(new Date(new Date().setHours(0)).setMinutes(0)).setSeconds(0)))
    const [dayMs, setDayMs] = useState(60000 * 60 * 24)
    const [open, setOpen] = useState(false)

    useEffect(() => {
        // if(Math.abs(revert)>1){
        //     Alert.alert("Need to purchase", "One day permission only!")
        //     setRevert(0)
        //     return;
        // }
        setLoading(true)
        setDate(new Date(todayFirst.getTime() - dayMs * Math.abs(revert)))
        const startDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + revert);
        const endDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1 + revert);
        
        const q = query(collection(firestore, "Token"), where("createdAt", ">=", startDate), where("createdAt", "<", endDate), orderBy("createdAt", "desc"));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            const data = snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    amount: i.data().amount,
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })
            setTokens(data)
            setLoading(false)
        });
    }, [revert])

    console.log(revert)

    return (
        <>
            <DatePicker
                modal
                open={open}
                date={date}
                mode="date"
                onConfirm={(date) => {
                    setOpen(false)
                    let dif = (new Date(date.toDateString()).getTime() - new Date(new Date().toDateString()).getTime()) / (1000 * 60 * 60 * 24)
                    let rev = dif > 0 ? Math.floor(dif) : Math.ceil(dif)
                    setRevert(rev)
                }}
                onCancel={() => {
                    setOpen(false)
                }}
                maximumDate={new Date()}
                dividerColor="#5a888f"
                buttonColor="#5a888f"
                title="Item Sale"
            />
            <View style={{ flex: 1, display: "flex" }}>
                <View style={{ flex: .08, display: "flex", flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 10, alignItems: "center", backgroundColor: "#ff2200" }} >
                    <TouchableOpacity onPress={() => setOpen(true)} style={{ backgroundColor: "#f52000", padding: 15, borderRadius: 5 }}>
                        <AntDesign name="calendar" size={32} color="white" />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { revert > -21 && setRevert((revert - 10)) }} disabled={!(revert > -21)} style={{ backgroundColor: "#f52000", padding: 15, borderRadius: 5 }}>
                        <AntDesign name="banckward" size={24} color={revert > -21 ? "white" : "#ff9e8f"} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { revert > -30 && setRevert((revert - 1)) }} disabled={!(revert > -30)} style={{ backgroundColor: "#f52000", padding: 15, borderRadius: 5 }}>
                        <AntDesign name="stepbackward" size={24} color={revert > -30 ? "white" : "#ff9e8f"} />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 20, fontWeight: "700", color: "#fff" }}>{date.toDateString().substring(4)}</Text>
                    <TouchableOpacity onPress={() => { revert < 0 && setRevert((revert + 1)) }} disabled={!(revert < 0)} style={{ backgroundColor: "#f52000", padding: 15, borderRadius: 5 }}>
                        <AntDesign name="stepforward" size={24} color={revert < 0 ? "white" : "#ff9e8f"} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { revert < -9 && setRevert((revert + 10)) }} disabled={!(revert < -9)} style={{ backgroundColor: "#f52000", padding: 15, borderRadius: 5 }}>
                        <AntDesign name="forward" size={24} color={revert < -9 ? "white" : "#ff9e8f"} />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: .91 }}>
                    {
                        loading ? <ActivityIndicator size="large" color="red" style={{display:"flex", flex:1, paddingLeft: 10, justifyContent:"center" }} /> :
                            tokens.length == 0 ? <Text style={{ padding: 20 }}>No items sold today!</Text> :
                                <TokenHistoryListComponent data={tokens} {...props} revert={revert}/>
                    }
                </View>
            </View>
        </>
    )
};
