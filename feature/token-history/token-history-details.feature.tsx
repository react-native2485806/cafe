import DataContext from "@/app/data-context";
import { RootState } from "@/config/redux";
import { ItemSale } from "@/config/type";
import { formatAMPM } from "@/util/util";
import { useContext } from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import { useSelector } from "react-redux";

export interface TokenHistoryDetailsFeatureProps {
    navigation: any
    route: any
}

export default function TokenHistoryDetailsFeature(props: TokenHistoryDetailsFeatureProps) {

    const { route } = props
    // const { itemSale, loading } = useContext(DataContext)
    const { itemSalesOfToken, loading } = useSelector((state: RootState) => state.catalogReducer)
    console.log(route)
    return (
        <>
            <View style={{ flex: 1 }}>
                {
                    loading ?
                        <ActivityIndicator size="large" color="red" style={{ display: "flex", flex: 1, paddingLeft: 10, justifyContent: "center" }} />
                        :
                        <>
                            <View style={styles.menu_header_container}>
                                <Text style={{ ...styles.menu_header, width: "35%" }}>{route?.params?.uuid}</Text>
                                <Text style={{ ...styles.menu_header, width: "40%" }}>{new Date(route?.params?.createdAt).toDateString()}</Text>
                                <Text style={{ ...styles.menu_header, width: "25%" }}>{route?.params?.uuid.substring(19)} {formatAMPM(new Date(route?.params?.createdAt))}</Text>
                            </View>
                            <View style={{ paddingHorizontal: 5 }}>
                                <View style={styles.header_container}>
                                    <Text style={{ ...styles.header, width: "10%" }}>Qty</Text>
                                    <Text style={{ ...styles.header, width: "10%" }}>X</Text>
                                    <Text style={{ ...styles.header, width: "55%" }}>Item (Price)</Text>
                                    <Text style={{ ...styles.header, width: "25%" }}>Total Price</Text>
                                </View>
                                {
                                    itemSalesOfToken.map((item: ItemSale) => (

                                        <View style={styles.data_container} key={item.uuid} >
                                            <Text style={{ ...styles.data, width: "10%" }}>{item.quantity}</Text>
                                            <Text style={{ ...styles.data, width: "10%" }}>X</Text>
                                            <Text style={{ ...styles.data, width: "55%" }}>{item.itemNameBn} ({item.price})</Text>
                                            <Text style={{ ...styles.data, width: "25%" }}>{item.quantity * item.price}</Text>
                                        </View>
                                    ))
                                }
                                {/* <View style={styles.sub_header_container}>
                        <Text style={{ ...styles.sub_header, width: "75%" }}>Sub Total</Text>
                        <Text style={{ ...styles.header, width: "25%" }}>4,800/=</Text>
                    </View>
                    <View style={styles.sub_header_container}>
                        <Text style={{ ...styles.sub_header, width: "75%" }}>Discount (10%)</Text>
                        <Text style={{ ...styles.header, width: "25%" }}>480/=</Text>
                    </View> */}
                                <View style={styles.sub_header_container}>
                                    <Text style={{ ...styles.sub_header, width: "75%" }}>Total</Text>
                                    <Text style={{ ...styles.header, width: "25%" }}>{route.params.amount}/=</Text>
                                </View>
                            </View>
                        </>
                }
            </View>
        </>
    )
};

const styles = StyleSheet.create({
    menu_header_container: {
        display: "flex",
        flexDirection: "row",
        backgroundColor: "white",
        paddingHorizontal: 10,
        paddingVertical: 20,
        // borderRadius: 5,
        marginBottom: 20,
        // justifyContent:"space-between",
    },
    menu_header: {
        // display: "flex",
        fontSize: 18,
        color: "#636363",
        fontWeight: "700",
        alignItems: "center",
        justifyContent: "center"
    },
    header_container: {
        display: "flex",
        flexDirection: "row",
        backgroundColor: "#f76b60",
        padding: 10,
        borderRadius: 5,
        // justifyContent:"space-between",
    },
    header: {
        // display: "flex",
        fontSize: 16,
        color: "white",
        fontWeight: "700",
        alignItems: "center",
        justifyContent: "center",
        padding: "auto"
    },
    data_container: {
        display: "flex",
        flexDirection: "row",
        backgroundColor: "white",
        padding: 10,
        borderRadius: 5,
        // justifyContent:"space-between",
        marginVertical: 4,
    },
    data: {
        // display: "flex",
        fontSize: 14,
        color: "#4d4d4d",
        fontWeight: "500",
        alignItems: "center",
        justifyContent: "center",
    },
    sub_header_container: {
        display: "flex",
        flexDirection: "row",
        backgroundColor: "#ffa29c",
        padding: 10,
        borderRadius: 5,
        marginVertical: 4
        // justifyContent:"space-between",
    },
    sub_header: {
        // display: "flex",
        fontSize: 16,
        color: "white",
        fontWeight: "700",
        alignItems: "center",
        justifyContent: "center",
        padding: "auto"
    },
})