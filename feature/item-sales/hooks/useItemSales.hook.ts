import { db } from "@/config/db"
import { firestore } from "@/config/firebaseConfig"
import { Category, ItemSale, WrappedItemSale } from "@/config/type"
import { collection, onSnapshot, orderBy, query, where } from "firebase/firestore"
import { useEffect, useState } from "react"
import { Alert } from "react-native"


export interface ItemSalesHookProps {
    navigation: any
    route: any
}

export interface ItemSalesHookInterface {
    revert: number,
    setRevert: (revert: number) => any,
    loading: boolean,
    total: number,
    todayFirst: Date,
    dayMs: number,
    date: Date,
    items: WrappedItemSale[],
    setDate: (date: Date) => void
}

export const useItemSalesHook = (props: ItemSalesHookProps): ItemSalesHookInterface => {

    const { navigation, route } = props
    const [revert, setRevert] = useState(route?.params?.revert || 0)
    const [loading, setLoading] = useState(false)
    const [total, setTotal] = useState(0)
    const [dayMs, setDayMs] = useState(60000 * 60 * 24)
    const [todayFirst, setTodayFirst] = useState(new Date(new Date(new Date(new Date().setHours(0)).setMinutes(0)).setSeconds(0)))
    const [date, setDate] = useState<Date>(new Date())
    const [items, setItems] = useState<WrappedItemSale[]>([])

    useEffect(() => {
        // if(Math.abs(revert)>1){
        //     Alert.alert("Need to purchase", "One day permission only!")
        //     setRevert(0)
        //     return;
        // }
        setLoading(true)
        getTotalSales()
        setDate(new Date(todayFirst.getTime() - dayMs * Math.abs(revert)))
    }, [revert])

    const getTotalSales = () => {

        setLoading(true)
        const startDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + revert);
        const endDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1 + revert);

        const q = query(collection(firestore, "ItemSale"), where("createdAt", ">=", startDate), where("createdAt", "<", endDate), orderBy("createdAt", "desc"));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            const salesOfTheDay: any = snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    ...i.data(),
                    itemId: (i.data().itemId as any)?._key.path.segments[6],
                    tokenId: (i.data().tokenId as any)?._key.path.segments[6],
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })

            let uuids = salesOfTheDay.map((i: any) => i.itemId)
            let uniqueUUIDs = Array.from(new Set(uuids))

            let quantitySalesByItem = uniqueUUIDs.map((itemId) => {
                let quantity = 0;
                let itemNameEn = '';
                let itemNameBn = '';
                let price = 0;
                let categoryId;
                for (let item of salesOfTheDay.filter((i: any) => i.itemId == itemId)) {
                    quantity = quantity + item.quantity
                    itemNameEn = item.itemNameEn
                    itemNameBn = item.itemNameBn
                    price = item.price
                    categoryId = item?.categoryId?._key?.path?.segments[6]
                }
                return { itemId, quantity, itemNameEn, itemNameBn, price, categoryId }
            })
            setLoading(false)
            setItems(quantitySalesByItem as any)
        });

    }

    return {
        revert,
        setRevert,
        loading,
        total,
        todayFirst,
        dayMs,
        date,
        setDate,
        items,
    }
}