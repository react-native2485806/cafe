import { SalesItemListComponent } from "@/components/item-list";
import { Text, TouchableOpacity, View } from "react-native";
import { useItemSalesHook } from "./hooks/useItemSales.hook";
import { AntDesign } from "@expo/vector-icons";
import DatePicker from 'react-native-date-picker'
import { useState } from "react";
import { ActivityIndicator } from "react-native";
export interface ItemSalesFeatureProps {
    navigation: any
    route: any
}

export default function ItemSalesFeature(props: ItemSalesFeatureProps) {

    const [open, setOpen] = useState(false)
    const { revert, setRevert, loading, total, dayMs, items, date, setDate } = useItemSalesHook({ ...props })

    return (
        <>
            <DatePicker
                modal
                open={open}
                date={date}
                mode="date"
                onConfirm={(date) => {
                    setOpen(false)
                    let dif = (new Date(date.toDateString()).getTime() - new Date(new Date().toDateString()).getTime()) / (1000 * 60 * 60 * 24)
                    let rev = dif > 0 ? Math.floor(dif) : Math.ceil(dif)
                    setRevert(rev)
                }}
                onCancel={() => {
                    setOpen(false)
                }}
                maximumDate={new Date()}
                dividerColor="#5a888f"
                buttonColor="#5a888f"
                title="Item Sale"
            />
            <View style={{ flex: 1, display: "flex" }}>
                <View style={{ flex: .08, display: "flex", flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 10, alignItems: "center", backgroundColor: "#ff2200" }} >
                    <TouchableOpacity onPress={() => setOpen(true)} style={{ backgroundColor: "#f52000", padding: 15, borderRadius: 5 }}>
                        <AntDesign name="calendar" size={32} color="white" />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { revert > -21 && setRevert((revert - 10)) }} disabled={!(revert > -21)} style={{ backgroundColor: "#f52000", padding: 15, borderRadius: 5 }}>
                        <AntDesign name="banckward" size={24} color={revert > -21 ? "white" : "#ff9e8f"} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { revert > -30 && setRevert((revert - 1)) }} disabled={!(revert > -30)} style={{ backgroundColor: "#f52000", padding: 15, borderRadius: 5 }}>
                        <AntDesign name="stepbackward" size={24} color={revert > -30 ? "white" : "#ff9e8f"} />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 20, fontWeight: "700", color: "#fff" }}>{date.toDateString().substring(4)}</Text>
                    <TouchableOpacity onPress={() => { revert < 0 && setRevert((revert + 1)) }} disabled={!(revert < 0)} style={{ backgroundColor: "#f52000", padding: 15, borderRadius: 5 }}>
                        <AntDesign name="stepforward" size={24} color={revert < 0 ? "white" : "#ff9e8f"} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { revert < -9 && setRevert((revert + 10)) }} disabled={!(revert < -9)} style={{ backgroundColor: "#f52000", padding: 15, borderRadius: 5 }}>
                        <AntDesign name="forward" size={24} color={revert < -9 ? "white" : "#ff9e8f"} />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: .91 }}>
                    {
                        loading ? <ActivityIndicator size="large" color="red" style={{display:"flex", flex:1, paddingLeft: 10, justifyContent:"center" }} /> :
                            items.length == 0 ? <Text style={{ padding: 20 }}>No items sold today!</Text> :
                                <SalesItemListComponent data={items} />
                    }
                </View>
            </View>
        </>
    )
};