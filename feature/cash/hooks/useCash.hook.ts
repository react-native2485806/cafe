import { db } from "@/config/db"
import { firestore } from "@/config/firebaseConfig"
import { collection, onSnapshot, orderBy, query, where } from "firebase/firestore"
import { useEffect, useState } from "react"
import { Alert } from "react-native"

export interface CashHookProps {

}

export interface CashHookInterface {
    revert: number,
    setRevert: (revert: number) => any,
    loading: boolean,
    totalSale: number,
    totalExpense: number,
    todayFirst: Date,
    dayMs: number,
    date: Date
    setDate: (date: Date) => void
}

export const useCashHook = (props: CashHookProps): CashHookInterface => {

    const [revert, setRevert] = useState(0)
    const [loading, setLoading] = useState(false)
    const [totalSale, setTotalSale] = useState(0)
    const [totalExpense, setTotalExpense] = useState(0)
    const [dayMs, setDayMs] = useState(60000 * 60 * 24)
    const [todayFirst, setTodayFirst] = useState(new Date(new Date(new Date(new Date().setHours(0)).setMinutes(0)).setSeconds(0)))
    const [date, setDate] = useState<Date>(new Date())



    useEffect(() => {
        // if(Math.abs(revert)>1){
        //     Alert.alert("Need to purchase", "One day permission only!")
        //     setRevert(0)
        //     return;
        // }
        setLoading(true)
        getTotalSales()
        getTotalExpense()
        setDate(new Date(todayFirst.getTime() - dayMs * Math.abs(revert)))
    }, [revert])

    const getTotalSales = () => {
        setLoading(true)
        const startDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + revert);
        const endDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1 + revert);

        const q = query(collection(firestore, "Token"), where("createdAt", ">=", startDate), where("createdAt", "<", endDate), orderBy("createdAt", "desc"));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            const salesOfTheDay = snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    amount: i.data().amount,
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })

            let total = 0
            for (let i of salesOfTheDay) {
                total = total + parseInt(i.amount)
            }
            setLoading(false)
            setTotalSale(total)
        });
    }

    const getTotalExpense = () => {
        setLoading(true)
        const startDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + revert);
        const endDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1 + revert);

        const q = query(collection(firestore, "Expense"), where("createdAt", ">=", startDate), where("createdAt", "<", endDate), orderBy("createdAt", "desc"));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            const expensesOfTheDay = snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    amount: i.data().amount,
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })

            let total: number = 0
            for (let i of expensesOfTheDay) {
                total = total + parseInt(i.amount)
            }
            setLoading(false)
            setTotalExpense(total)
        });
    }

    return {
        revert,
        setRevert,
        loading,
        totalSale,
        totalExpense,
        todayFirst,
        dayMs,
        date,
        setDate
    }
}