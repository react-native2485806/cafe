import { Text } from "@rneui/base";
import { View } from "react-native";

export interface OrderDetailsFeatureProps {
    navigation: any
    route: any
}

export default function OrderDetailsFeature(props: OrderDetailsFeatureProps) {

    const { navigation, route } = props

    return (
        <View>
            <Text>
                Order details feature
            </Text>
        </View>
    )
};
