import { RootState } from "@/config/redux";
import { Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux"
import UUID from 'react-native-uuid';
import { db } from "@/config/db";
import ThermalPrinterModule from 'react-native-thermal-printer';
import { updateItemSale, updateRefreshCatalog, updateToken } from "@/feature/catalog/redux/catalogReducer";
import { SelectedMenu } from "@/config/type";
import { deleteData, setData } from "@/config/apis";
import { collection, doc, onSnapshot, query, where } from "firebase/firestore";
import { firestore } from "@/config/firebaseConfig";
// import { Html5Entities } from 'html-entities'; 

export interface PrintHookProps {

}

export interface PrintHookInterface {
    onClickPrint: (
        uuid: string,
        headerRef: any,
        firstBatchDataRef: any,
        summaryRef: any,
        secondBatchDataRef: any,
        thirdBatchDataRef: any,
        localSelectedMenu: any,
        setLocalSelectedMenu: any
    ) => void
}

export const usePrintHook = (props: PrintHookProps): PrintHookInterface => {

    const dispatch = useDispatch()
    const { selectedMenu, total, printerSettings: { printingLanguage } } = useSelector((state: RootState) => state.catalogReducer)

    const fetchItemSales = () => {
        db.transaction((tx) => {
            tx.executeSql(`SELECT * from ItemSale ORDER BY createdAt ASC`, [],
                (tx, res) => { dispatch(updateItemSale(res.rows._array)) }
            );
        });
    }

    const fetchTokens = () => {
        db.transaction((tx) => {
            tx.executeSql(`SELECT * from Token ORDER BY createdAt DESC`, [],
                (tx, res) => { dispatch(updateToken(res.rows._array)) }
            );
        });
    }

    const deleteTokenAndItemSales = async (uuid: string) => {
        db.transaction(async (tx) => {
            tx.executeSql(`DELETE FROM Token WHERE uuid="${uuid}"`)
            tx.executeSql(`DELETE FROM ItemSale WHERE tokenId="${uuid}"`)
        })
    }

    const deleteToken = (uuid: string) => {
        deleteData("Token", uuid)
        const q = query(collection(firestore, "ItemSale"), where("tokenId", "==", doc(firestore, "Token", uuid)));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            snapshot.docs.forEach((i) => {
                deleteData("ItemSale", i.id)
            })
        });
    }

    const generateEnglishPrintPaper = (date: Date, items: any[], uuid: string) => {
        let total = 0
        for (let i of items) {
            total = total + (parseInt(i.price) * parseInt(i.quantity))
        }
        let textHead = "[C]<font size='normal'><b>Vista Cafe</b></font>\n"
            + "[C]NSTU, Shanti Niketon\n"
            + "[C]" + date.toDateString() + " " + date.toLocaleTimeString() + "\n"
            + "[L]Order Id # <b>" + uuid.substring(uuid.length - 12, uuid.length) + "</b>\n"
            + "[L]Qty   Item[R]Price\n"
            + "--------------------------------\n"
        let textTail = "--------------------------------\n[L]<b>Total</b>[R]" + total.toFixed(2)
        let text = textHead
        for (let i of items) text = text + "[L]" + i.quantity.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }) + " X  " + i.name + "[R]" + (parseInt(i.price) * parseInt(i.quantity)).toFixed(2) + "\n"
        return text + textTail
    }

    const printEnglish = async (date: Date, uuid: string, headerRef: any, firstBatchDataRef: any, secondBatchDataRef: any, thirdBatchDataRef: any, summaryRef: any, setLocalSelectedMenu: any, localSelectedMenu: any) => {
        try {
            let payload = generateEnglishPrintPaper(date, localSelectedMenu, uuid)
            setLocalSelectedMenu([])
            await ThermalPrinterModule.printBluetooth({
                payload,
                printerNbrCharactersPerLine: 32,
            }).then(() => {
            });
        } catch (err: any) {
            deleteToken(uuid)
            Alert.alert("Failed!", err.message)
        }
    }

    const print = async (date: Date, uuid: string, headerRef: any, firstBatchDataRef: any, secondBatchDataRef: any, thirdBatchDataRef: any, summaryRef: any, setLocalSelectedMenu: any) => {
        try {
            let headerUri = await headerRef.current.capture()
            let header = `<img>${headerUri}</img>\n`
            let summaryUri = await summaryRef.current.capture()
            let summary = `\n<img>${summaryUri}</img>`
            let payload = ``
            console.log(selectedMenu.length <= 3)
            if (selectedMenu.length <= 3) payload = header + `<img>${await firstBatchDataRef.current.capture()}</img>`
            if (selectedMenu.length <= 5 && selectedMenu.length > 3) payload = header + `<img>${await secondBatchDataRef.current.capture()}</img>`
            if (selectedMenu.length <= 8 && selectedMenu.length > 5) payload = header + `<img>${await thirdBatchDataRef.current.capture()}</img>`
            if (selectedMenu.length <= 11 && selectedMenu.length > 8) payload = header + `<img>${await firstBatchDataRef.current.capture()}</img>\n` + `<img>${await thirdBatchDataRef.current.capture()}</img>`
            if (selectedMenu.length <= 13 && selectedMenu.length > 11) payload = header + `<img>${await secondBatchDataRef.current.capture()}</img>\n` + `<img>${await thirdBatchDataRef.current.capture()}</img>`
            setLocalSelectedMenu([])
            await ThermalPrinterModule.printBluetooth({
                payload: payload + summary,
                printerNbrCharactersPerLine: 38,
            }).then(() => {
            });
        } catch (err: any) {
            deleteToken(uuid)
            Alert.alert("Failed!", err.message)
        }
    }

    const generateItemSale = async (date: Date, uuid: string, localSelectedMenu: SelectedMenu[]) => {
        try {
            try {
                setData("Token", uuid, {
                    amount: total,
                    createdAt: date
                })
            } catch (error) {
                alert('Check internet connection!')
            }

            for (let item of localSelectedMenu ?? []) {

                console.log("saling item:", item)
                let itemUUID = UUID.v4()

                setData("ItemSale", itemUUID.toString(), {
                    itemId: doc(firestore, "/Item/" + item.uuid),
                    itemNameBn: item.nameBn,
                    itemNameEn: item.name,
                    tokenId: doc(firestore, "/Token/" + uuid),
                    categoryId: doc(firestore, "/Category/" + item.categoryId),
                    price: item.price,
                    quantity: item.quantity,
                    createdAt: date
                })
            }
        } catch (e) { }
    }

    const onClickPrint = (uuid: string, headerRef: any, firstBatchDataRef: any, secondBatchDataRef: any, thirdBatchDataRef: any, summaryRef: any, localSelectedMenu: any, setLocalSelectedMenu: any) => {
        if (!selectedMenu.length) {
            Alert.alert("Sorry!", "No items is selected!")
        } else if (selectedMenu.length > 13) {
            Alert.alert("Sorry!", "Please select maximum 14 items!")
        }
        else {
            let date = new Date();
            // let date = new Date(new Date().getTime()-60000*60*24*6);
            dispatch(updateRefreshCatalog())
            generateItemSale(date, uuid.toString(), localSelectedMenu)
            printingLanguage === 'bn' ?
                print(date, uuid, headerRef, firstBatchDataRef, secondBatchDataRef, thirdBatchDataRef, summaryRef, setLocalSelectedMenu) :
                printEnglish(date, uuid, headerRef, firstBatchDataRef, secondBatchDataRef, thirdBatchDataRef, summaryRef, setLocalSelectedMenu, localSelectedMenu)
        }
    }

    return {
        onClickPrint
    }
}