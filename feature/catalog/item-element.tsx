import { AntDesign, Entypo, MaterialIcons } from "@expo/vector-icons";
import { useEffect, useState } from "react";
import { StyleSheet, Text, TextInput, TouchableHighlight, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { updateSelectedMenu } from "./redux/catalogReducer";
import { RootState } from "@/config/redux";

export const ItemElement = ({ item, counter }: any) => {

    const dispatch = useDispatch();
    const [quantity, setQuantity] = useState(0);
    const selectedCategory = useSelector((state: RootState) => state.catalogReducer.selectedCategory);
    const refreshCatalog = useSelector((state: RootState) => state.catalogReducer.refreshCatalog);

    useEffect(() => {
        dispatch(updateSelectedMenu({
            uuid: item.uuid,
            quantity: quantity,
            price: item.price,
            name: item.name,
            nameBn: item.nameBn,
            categoryId: item.categoryId,
        }))
    }, [quantity])

    useEffect(() => {
        if (quantity > 0) {
            setQuantity(0);
            dispatch(updateSelectedMenu({
                uuid: item.uuid,
                quantity: 0,
                price: item.price,
                name: item.name,
                nameBn: item.nameBn,
                categoryId: item?.categoryId,
            }))
        }
    }, [refreshCatalog])

    return (
        <>
            <TouchableOpacity style={{
                backgroundColor: "white",
                borderRadius: 5,
                margin: 4,
                padding: 8,
                display: selectedCategory == item.categoryId ? "flex" : "none"
            }}
                onPress={() => { setQuantity(quantity + 1); }}
            >
                <View key={item.uuid} style={{
                    backgroundColor: quantity > 0 ? "#ffab66" : "white",
                    borderRadius: 5,
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    height: 40,
                    padding: 5
                }}
                >
                    <View style={{
                        flexDirection: "row"
                    }}>
                        <MaterialIcons name="menu-book" size={24} color={quantity > 0 ? "white" : "#5a888f"} style={{ alignSelf: "center" }} />
                        <Text style={{ color: quantity > 0 ? "white" : "black", alignSelf: "center", fontSize: 16, paddingHorizontal: 10 }}>{item.nameBn}</Text>
                    </View>
                    <View style={{
                        flexDirection: "row"
                    }}>
                        <TouchableHighlight
                            activeOpacity={0.6}
                            underlayColor="#DDDDDD"
                            onPress={() => quantity > 0 && setQuantity(quantity - 1)}
                            style={{ paddingHorizontal: 10 }}>
                            <AntDesign name="minus" size={30} color={quantity > 0 ? "white" : "#ffab66"} />
                        </TouchableHighlight>
                        <TextInput
                            style={{ ...styles.input, borderColor: quantity > 0 ? "white" : "#ffab66", color: quantity > 0 ? "white" : "#ffab66", backgroundColor: quantity > 0 ? "#ffab66" : "white" }}
                            value={quantity.toString()}
                            placeholder="useless placeholder"
                            keyboardType="numeric"
                            maxLength={3}
                            defaultValue="0"
                            onChangeText={(v) => { setQuantity(parseInt(v ? v : "0")) }}

                        />
                        <TouchableHighlight
                            activeOpacity={0.6}
                            underlayColor="#DDDDDD"
                            onPress={() => setQuantity(0)}
                            style={{ paddingHorizontal: 10 }}>
                            <Entypo name="cross" size={30} color={quantity > 0 ? "white" : "#ffab66"} />
                        </TouchableHighlight>
                    </View>
                </View>
            </TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
    input: {
        height: 35,
        borderWidth: 2,
        padding: 2,
        color: "red",
        width: 50,
        fontSize: 18,
        borderColor: "#ffab66",
        borderRadius: 5,
        textAlign: "center",
        fontWeight: "800", alignSelf: "center"
    },
});
