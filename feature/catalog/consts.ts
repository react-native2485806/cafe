

export const itemCategories = [
    {
        key: "1",
        title: "Appetizers",
        titleBn: "ক্ষুধাবর্ধক খাবার",
        order: 1
    },
    {
        key: "2",
        title: "Entrees",
        titleBn: "প্রধান খাবার",
        order: 2
    },
    {
        key: "3",
        title: "Skeyes",
        titleBn: "পার্শ্ব খাবার",
        order: 3
    },
    {
        key: "4",
        title: "Desserts",
        titleBn: "ডেজার্ট",
        order: 4
    },
    {
        key: "5",
        title: "Drinks",
        titleBn: "পানীয়",
        order: 5
    },
    {
        key: "6",
        title: "Fast Food",
        titleBn: "ফাষ্ট ফুড",
        order: 6
    },
]

export const items = [
    {
        id: 1,
        name: "Boiled Rice",
        nameBn: "ভাত",
        price: "50",
        order: 1
    },
    {
        id: 2,
        name: "Beef ",
        nameBn: "গরুর গোশত",
        price: "50",
        order: 2
    },
    {
        id: 3,
        name: "Cutlet",
        nameBn: "মাংসের বড়া",
        price: "50",
        order: 3
    },
    {
        id: 4,
        name: "Fowl",
        nameBn: "মুরগির মাংস",
        price: "50",
        order: 4
    },
    {
        id: 5,
        name: "Egg",
        nameBn: "ডিম",
        price: "50",
        order: 5
    },
    {
        id: 6,
        name: "Hotch Potch",
        nameBn: "কিচুড়ি",
        price: "50",
        order: 6
    },
    {
        id: 7,
        name: "Fish",
        nameBn: "মাছ",
        price: "50",
        order: 7
    },
    {
        id: 8,
        name: "Mutton",
        nameBn: "খাসির মাংস",
        price: "50",
        order: 8
    },
    {
        id: 9,
        name: "Pulse",
        nameBn: "ডাল",
        price: "50",
        order: 9
    },
    {
        id: 10,
        name: "Pillau",
        nameBn: "পোলাও",
        price: "50",
        order: 10
    },
]