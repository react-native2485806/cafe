import { ActivityIndicator, Alert, ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from "react-native";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@/config/redux";
import { updateSelectedCategory } from "./redux/catalogReducer";
import { getData } from "@/config/apis";
import { firestore } from "@/config/firebaseConfig";
import { collection, onSnapshot, query } from "firebase/firestore";

export interface CatalogListHeaderFeatureProps {
    navigation: any
    route: any
}

export default function CatalogListHeaderFeature({ navigation, route }: CatalogListHeaderFeatureProps) {

    const dispatch = useDispatch()
    const [category, setCategory] = useState<any[]>([])
    const [loading, setLoading] = useState(false)

    const selectedCategory = useSelector((state: RootState) => state.catalogReducer.selectedCategory);

    useEffect(() => {
        setLoading(true)
        const q = query(collection(firestore, "Category"));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            setCategory(snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    name: i.data().name,
                    nameBn: i.data().nameBn,
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            }))
            setLoading(false)

        });
    }, [setCategory])

    useEffect(() => {
        category.length && dispatch(updateSelectedCategory(category[0]?.uuid))
    }, [category])

    return (
        <>
            <View>
                
                    <ScrollView
                        style={{ maxHeight: 73, borderRadius: 5, backgroundColor: "white", padding: 5, marginTop: 5, marginBottom: 8 }}
                        horizontal={true}
                        contentContainerStyle={styles.tabBar}
                        showsHorizontalScrollIndicator={false}
                    >
                        {loading ? <ActivityIndicator size="small" color="red" style={{ paddingLeft: 10 }} /> :<>
                        {category.map((cat, i) => (
                            <TouchableOpacity
                                key={cat.uuid}
                                onPress={() => { dispatch(updateSelectedCategory(cat.uuid)) }}
                                style={[
                                    styles.tabItem,
                                    selectedCategory === cat.uuid && styles.selectedTabItem,
                                ]}
                            >
                                <Text style={{ color: "white" }}>{cat.nameBn}</Text>
                            </TouchableOpacity>
                        ))}
                        </>
                        }
                    </ScrollView>
            </View>
        </>
    )
};


const styles = StyleSheet.create({
    tabBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white",
        minWidth: "100%",
        borderRadius: 5,
        maxHeight: 60,
        padding: 5,
    },
    tabItem: {
        paddingVertical: 12,
        paddingHorizontal: 15,
        backgroundColor: "#ffccb5",
        marginHorizontal: 2,
        borderRadius: 5,
        alignItems: "center"
    },
    selectedTabItem: {
        backgroundColor: "#f08554",
    },
});