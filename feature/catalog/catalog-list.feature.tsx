import { Alert, FlatList, Text, TouchableHighlight, View } from "react-native";
import { Avatar, ListItem } from '@rneui/themed';
import { Entypo } from '@expo/vector-icons';
import { items } from "./consts";
import CatalogListContainerFeature from "./catalog-list-container.feature";
import CatalogListHeaderFeature from "./catalog-list-header.feature";

export interface CatalogListFeatureProps {
    navigation: any
    route: any
}

export default function CatalogListFeature(props: CatalogListFeatureProps) {
    const { } = props

    return (
        <>
            <CatalogListHeaderFeature  {...props} />
            <CatalogListContainerFeature {...props} />
        </>
    )
};
