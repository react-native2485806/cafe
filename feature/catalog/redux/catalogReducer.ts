// catalogReducer.ts
import { Category, Item, ItemSale, SelectedMenu, Token, WrappedItemSale } from '@/config/type';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';


interface catalogState {
    sqlData: {
        category: Category[];
        item: Item[];
        token: Token[];
        itemSale: ItemSale[];
    }
    selectedMenu: SelectedMenu[];
    selectedCategory: string;
    wrappedItemSale: WrappedItemSale[];
    itemSalesOfToken: ItemSale[];
    total: number;
    totalOfToday: number;
    counter: number;
    loading: boolean;
    refreshCatalog: number;
    printerSettings: {
        printingLanguage: 'bn' | 'en'
    }
}

const initialState: catalogState = {
    sqlData: {
        category: [],
        item: [],
        token: [],
        itemSale: [],
    },
    selectedMenu: [],
    selectedCategory: '',
    wrappedItemSale: [],
    itemSalesOfToken: [],
    total: 0,
    totalOfToday: 0,
    counter: 0,
    loading: false,
    refreshCatalog: 0,
    printerSettings: {
        printingLanguage: 'bn'
    }
};

const catalogSlice = createSlice({
    name: 'catalogs',
    initialState,
    reducers: {
        updateCategory(state, action: PayloadAction<Category[]>) {
            state.sqlData.category = action.payload
        },
        updateItem(state, action: PayloadAction<Item[]>) {
            state.sqlData.item = action.payload
        },
        updateToken(state, action: PayloadAction<Token[]>) {
            state.sqlData.token = action.payload
        },
        updateItemSale(state, action: PayloadAction<ItemSale[]>) {
            const itemSales = action.payload
            state.sqlData.itemSale = itemSales

            let salesOfToday = itemSales.filter((i) => new Date(i.createdAt as any).getTime() > (new Date().setHours(0) - (60000 * 60 * 6)))
            let uuids = salesOfToday.map((i) => i.itemId)
            let uniqueUUIDs = Array.from(new Set(uuids))
            let quantitySalesByItem = uniqueUUIDs.map((itemId) => {
                let quantity = 0;
                let itemNameEn = '';
                let itemNameBn = '';
                let price = 0;
                for (let item of salesOfToday.filter((i) => i.itemId == itemId)) {
                    quantity = quantity + item.quantity
                    itemNameEn = item.itemNameEn
                    itemNameBn = item.itemNameBn
                    price = item.price
                }
                return { itemId, quantity, itemNameEn, itemNameBn, price }
            })
            state.wrappedItemSale = quantitySalesByItem

            let totalOfToday = 0
            for (let i of quantitySalesByItem) {
                totalOfToday = totalOfToday + (i.price * i.quantity)
            }
            state.totalOfToday = totalOfToday
        },
        updateSelectedMenu(state, action: PayloadAction<SelectedMenu>) {
            let quantity = action.payload.quantity
            let uuid = action.payload.uuid

            let isExist = (state.selectedMenu.filter((sm) => sm.uuid == uuid)).length
            if (isExist) {
                if (quantity == 0) state.selectedMenu = state.selectedMenu.filter((sm) => sm.uuid != uuid)
                else {
                    state.selectedMenu = state.selectedMenu.map((sm) => {
                        if (sm.uuid == uuid) return { ...sm, uuid: sm.uuid, quantity: quantity }
                        else return sm
                    }
                    )
                }
            } else {
                if (quantity !== 0) state.selectedMenu.push(action.payload)
            }
            let total = 0
            for (let i of state.selectedMenu) {
                total = total + i.price * i.quantity
            }
            state.total = total
        },
        resetSelectedMenu(state) {
            state.selectedMenu = []
            state.total = 0
        },
        updateSelectedCategory(state, action: PayloadAction<String>) {
            state.selectedCategory = action.payload.valueOf()
        },
        updateItemSalesOfToken(state, action: PayloadAction<ItemSale[]>) {
            state.itemSalesOfToken = action.payload
            state.loading = false
        },
        updateCounter(state) {
            state.counter = state.counter + 1
        },
        updateLoading(state, action: PayloadAction<boolean>) {
            state.loading = action.payload
        },
        updateRefreshCatalog(state) {
            state.refreshCatalog = state.refreshCatalog + 1
        },
        updatePrinterLanguage(state, action: PayloadAction<'bn' | 'en'>) {
            state.printerSettings.printingLanguage = action.payload
        },
    },
});

export const {
    updateCategory,
    updateItem,
    updateToken,
    updateItemSale,
    updateSelectedMenu,
    updateSelectedCategory,
    resetSelectedMenu,
    updateItemSalesOfToken,
    updateCounter,
    updateLoading,
    updateRefreshCatalog,
    updatePrinterLanguage
} = catalogSlice.actions;

export default catalogSlice.reducer;