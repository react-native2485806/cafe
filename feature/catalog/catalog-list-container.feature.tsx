import { ActivityIndicator, ScrollView, View } from "react-native";
import { useEffect, useState } from "react";
import { ItemElement } from "./item-element";
import { collection, onSnapshot, query } from "firebase/firestore";
import { firestore } from "@/config/firebaseConfig";

export interface CatalogListContainerFeatureProps {
    navigation: any
    route: any
}

export default function CatalogListContainerFeature(props: CatalogListContainerFeatureProps) {

    const [item, setItem] = useState<any[]>([])
    const [loading, setLoading] = useState(true)

    const fetchItem = () => {
        console.log("calling...")
        const q = query(collection(firestore, "Item"));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            const data = snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    categoryId: (i.data()?.categoryId as any)?._key?.path.segments[6],
                    name: i.data().name,
                    nameBn: i.data().nameBn,
                    price: i.data().price,
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })
            setItem(data)
            data.length && setLoading(false)
        });
    }

    useEffect(() => {
        fetchItem()
    },[])

    return (
        <>
            {
                loading ? <ActivityIndicator size="large" color="red" style={{ display: "flex", flex: 1, paddingLeft: 10, justifyContent: "center" }} /> :
                    <View>
                        <ScrollView>
                            <>{item.map((i, index: number) => <ItemElement key={i.uuid} item={i} />)}</>

                        </ScrollView>
                    </View>
            }
        </>
    )
};
