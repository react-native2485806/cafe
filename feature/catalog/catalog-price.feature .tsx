import { Alert, Button, FlatList, Pressable, StyleSheet, Text, TextInput, TouchableHighlight, View } from "react-native";
import { Avatar, Divider, ListItem } from '@rneui/themed';
import { data } from "../../util/temp";
import { Foundation } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { useState } from "react";
import { Fontisto } from '@expo/vector-icons';
import Carousel from 'react-native-snap-carousel';
import { items } from "./consts";

export interface CatalogPriceFeatureProps {
    setShowItem: (v: boolean) => void;
    quantity: number,
    setQuantity: (v: number) => void
}

export default function CatalogPriceFeature({ setShowItem, quantity, setQuantity }: CatalogPriceFeatureProps) {

    return (
        <>
            <View style={{ width: "70%" }}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <TouchableHighlight
                        activeOpacity={0.6}
                        underlayColor="#DDDDDD"
                        onPress={() => setShowItem(false)}
                        style={{}}>
                        <Entypo name="cross" size={30} color="red" />
                    </TouchableHighlight>
                    <Text style={{ padding: 10, fontSize: 16, fontWeight: "500", textAlign: "center" }}>
                        Alba King Alba King Alba King
                    </Text>
                </View>
                <View style={{ padding: 10, paddingLeft: 0, display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                    <TouchableHighlight
                        disabled={quantity == 0}
                        activeOpacity={0.6}
                        underlayColor="#DDDDDD"
                        onPress={() => { quantity > 0 && setQuantity(quantity - 1) }}
                        style={{ padding: 16, borderRadius: 50, backgroundColor: "#e6e6e6" }}>
                        <AntDesign name="minussquareo" size={24} color="#ffab66" />
                    </TouchableHighlight>
                    <TextInput
                        style={{
                            height: 40,
                            width: 40,
                            margin: 5,
                            padding: 2,
                            borderWidth: 1,
                            fontSize: 20,
                            borderRadius: 5,
                            fontWeight: "600",
                            color: "red",
                            textAlign: "center",
                            borderColor: "red"
                        }}
                        onChangeText={(v) => { setQuantity(parseInt(v ? v : "0")) }}
                        value={quantity.toString()}
                        placeholder="qty"
                        keyboardType="numeric"
                        maxLength={3}
                        defaultValue="0"
                    />
                    <TouchableHighlight
                        disabled={quantity == 999}
                        activeOpacity={0.6}
                        underlayColor="#DDDDDD"
                        onPress={() => { quantity < 999 && setQuantity(quantity + 1) }}
                        style={{ padding: 16, borderRadius: 50, backgroundColor: "#e6e6e6" }}>
                        <AntDesign name="plussquareo" size={24} color="#5fcfe3" />
                    </TouchableHighlight>
                </View>
            </View>
            <View style={{ paddingVertical: 10, display: "flex", width: "30%", alignItems: "flex-end" }}>
                <View style={{ width: "100%", display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={{ fontSize: 14, fontWeight: "500" }}>
                        Price:
                    </Text>
                    <Text style={{ fontSize: 14, fontWeight: "500" }}>
                        50
                    </Text>
                </View>
                <View style={{ width: "100%", display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={{ fontSize: 14, fontWeight: "500", alignItems: "flex-start", display: "flex" }}>
                        Amount:
                    </Text>
                    <Text style={{ color: "red", fontSize: 14, fontWeight: "500", justifyContent: "flex-end", display: "flex" }}>
                        5
                    </Text>
                </View>
                <View style={{ width: "100%", display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={{ fontSize: 14, fontWeight: "500", alignItems: "flex-start", display: "flex" }}>
                        Discount:
                    </Text>
                    <Text style={{ fontSize: 14, fontWeight: "500", justifyContent: "flex-end", display: "flex" }}>
                        0
                    </Text>
                </View>
                <Divider inset={true} subHeaderStyle={{ color: "black" }} />
                <View style={{ width: "100%", display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={{ fontSize: 18, fontWeight: "700", alignItems: "flex-start", display: "flex" }}>
                        Total:
                    </Text>
                    <Text style={{ fontSize: 18, fontWeight: "700", alignSelf: "flex-end", display: "flex" }}>
                        250
                    </Text>
                </View>
            </View>
        </>
    )
};
