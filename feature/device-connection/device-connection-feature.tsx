
import { Text } from "@rneui/base";
import { useContext, useState } from "react";
import { Alert, Button, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, View } from "react-native";
import { FontAwesome } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import DataContext from "@/app/data-context";
import { db } from "@/config/db";
import * as Network from 'expo-network';

export interface DeviceConnectionFeatureProps {

}

export default function DeviceConnectionFeature(params: DeviceConnectionFeatureProps) {


    const [color, setColor] = useState('white')
    const [selectedIndex, setSelectedIndex] = useState(0)

    const { onClickPrint } = useContext(DataContext);

    const check = async () => {
        // console.log(await Network.getNetworkStateAsync())
        // const isEnabled = await BluetoothManager.checkBluetoothEnabled();
        // console.log(isEnabled);
    }


    return (<View style={{ flex: 1 }}>

        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <TouchableWithoutFeedback
                    onPressIn={() => { setColor('#db7e32'), setSelectedIndex(1) }}
                    onPressOut={() => { setColor('white'), setSelectedIndex(0) }}
                    onPress={() => { Alert.alert("Connection to this printer.") }}
                >
                    <View style={{ padding: 20, height: 200, width: '40%', margin: 20, backgroundColor: selectedIndex == 1 ? color : 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <MaterialCommunityIcons name="cloud-print-outline" style={{}} size={60} color={selectedIndex == 1 ? 'white' : "#db7e32"} />
                        <Text style={{ marginTop: 10, color: selectedIndex == 1 ? 'white' : "#db7e32" }}>
                            Printer 1
                        </Text>
                        <Text style={{ marginTop: 2, color: selectedIndex == 1 ? 'white' : "#db7e32", fontWeight:"800" }}>
                            Connected
                        </Text>
                    </View>
                </TouchableWithoutFeedback >
                <TouchableWithoutFeedback
                    onPressIn={() => { setColor('#db7e32'), setSelectedIndex(2) }}
                    onPressOut={() => { setColor('white'), setSelectedIndex(0) }}
                    onPress={() => { Alert.alert("Connection to this printer.") }}
                >
                    <View style={{ padding: 20, height: 200, width: '40%', margin: 20, backgroundColor: selectedIndex == 2 ? color : 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <MaterialCommunityIcons name="cloud-print-outline" style={{}} size={60} color={selectedIndex == 2 ? 'white' : "#db7e32"} />

                        <Text style={{ marginTop: 10, color: selectedIndex == 2 ? 'white' : "#db7e32" }}>
                            Printer 2
                        </Text>
                        <Text style={{ marginTop: 2, color: selectedIndex == 2 ? 'white' : "#db7e32" }}>
                            Disconnected
                        </Text>
                    </View>
                </TouchableWithoutFeedback >
            </View>
            {/* <Text style={{padding:20, color:"#fc9a58", fontWeight:"700"}}>Notice: Printer ABC connected</Text> */}
            <View style={{ display: "flex", justifyContent: "space-evenly", flexDirection: "row", padding:10 }}>
                <TouchableOpacity
                    onPress={check}
                    style={{ width: "45%", backgroundColor: "#ff6647", padding: 10, marginHorizontal: 10, borderRadius: 5, alignItems: "center" }}
                >
                    <Text style={{ color: "white", fontSize: 20, fontWeight: "700" }}>View Details</Text>
                </TouchableOpacity>
                {/* <TouchableOpacity
                    onPress={onClickPrint}
                    style={{ width: "45%", backgroundColor: "#ff6647", padding: 10, marginHorizontal: 10, borderRadius: 5, alignItems: "center" }}
                >
                    <Text style={{ color: "white", fontSize: 20, fontWeight: "700" }}>Print</Text>
                </TouchableOpacity> */}

            </View>
        </View >
    </View>
    )

};

const styles = StyleSheet.create({
    heading: {
        fontSize: 18,
        fontWeight: '600',
        marginBottom: 13,
    },
    card: {
        backgroundColor: 'white',
        borderRadius: 8,
        paddingVertical: 45,
        paddingHorizontal: 25,
        width: '100%',
        marginVertical: 10,
    },
    shadowProp: {
        shadowColor: '#171717',
        shadowOffset: { width: -2, height: 4 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
    },
});
