import DefaultInfoComponent from "@/components/info/default-info.component";


interface ItemDetailsFeatureProps {
    navigation: any
}

export default function ItemDetailsFeature(params: ItemDetailsFeatureProps) {


    return (
        <DefaultInfoComponent navigation={params.navigation}/>
    )
};
