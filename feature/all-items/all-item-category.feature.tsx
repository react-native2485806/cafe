import { DefaultItemListComponent } from "@/components/item-list";
import { useAllItemHook } from "./hooks/useAllItem.hook";
import { useEffect, useState } from "react";
import { getData } from "@/config/apis";
import { Text } from "react-native";

interface AllItemCategoryFeatureProps {
    navigation: any
}

export default function AllItemCategoryFeature(params: AllItemCategoryFeatureProps) {

    const [categories, setCategories] = useState([])
    const [loading, setLoading] = useState<boolean>(true)

    useEffect(() => {
        getData("Category").then((p) => {
            setCategories(p)
            setLoading(false)
            // var t = new Date(Date.UTC(1970, 0, 1));
            // console.log("--------",new Date(p[0].createdAt.seconds*1000))
        })
    }, [])

    const { } = useAllItemHook({})

    return (
        loading ?
            <Text>
                Loading...
            </Text> :
            <DefaultItemListComponent data={categories} navigation={params.navigation} to="Items" />
    )
};
