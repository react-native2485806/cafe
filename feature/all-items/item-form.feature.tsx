import { SaveItemFormComponent } from "@/components/form";
import DefaultInfoComponent from "@/components/info/default-info.component";
import { KeyboardAvoidingView, ScrollView, View } from "react-native";
import { Ionicons } from '@expo/vector-icons';

interface ItemFormFeatureProps {
    navigation: any,
    route: any,
}

export default function ItemFormFeature(params: ItemFormFeatureProps) {

    return (
        <ScrollView style={{ display: "flex", flex: 1, backgroundColor: "white", margin: 10, paddingTop: 30, borderRadius: 5 }}>
            <KeyboardAvoidingView behavior='height'>
                <View style={{ paddingBottom: 30 }}>
                    <Ionicons name="fast-food" size={140} color="#6ab0a3" style={{ display: "flex", alignSelf: "center" }} />
                    <SaveItemFormComponent {...params} />
                </View>
            </KeyboardAvoidingView>
        </ScrollView>
    )
};
