import { useEffect, useState } from "react";
import { Text, View } from "react-native";
import ItemListContainerFeature from "./item-list-container.feature";
import { db } from "@/config/db";
import { useFocusEffect } from "@react-navigation/native";

interface AllItemFeatureProps {
    navigation: any
    route: any
}

export default function AllItemFeature(params: AllItemFeatureProps) {

    return (
        <>
            <View style={{ flex: .9, flexDirection: 'column' }}>
                <View style={{ flex: 9, margin: 10, marginTop: 0, borderRadius: 5 }}>
                    <ItemListContainerFeature navigation={params.navigation} />
                </View>
            </View >
        </>
    )
};
