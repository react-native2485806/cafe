import { FlatList, ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View, useWindowDimensions } from "react-native";
import { useEffect, useState } from "react";
import { Item } from "@/config/type";
import { Avatar, ListItem } from "@rneui/themed";
import { Entypo } from "@expo/vector-icons";
import { getData } from "@/config/apis";
import { collection, onSnapshot, query } from "firebase/firestore";
import { firestore } from "@/config/firebaseConfig";

export interface ItemListContainerFeatureProps {
    navigation: any;
}

export default function ItemListContainerFeature({ navigation }: ItemListContainerFeatureProps) {

    const [index, setIndex] = useState(0);
    const [category, setCategory] = useState<any[]>([])
    const [item, setItem] = useState<any[]>([])

    useEffect(() => {

        const cq = query(collection(firestore, "Category"));
        onSnapshot(cq, { includeMetadataChanges: true }, (snapshot) => {
            const data = snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    name: i.data().name,
                    nameBn: i.data().nameBn,
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })
            setCategory(data)
        });

        const q = query(collection(firestore, "Item"));
        onSnapshot(q, { includeMetadataChanges: true }, (snapshot) => {
            const data = snapshot.docs.map((i) => {
                return {
                    uuid: i.id,
                    categoryId: i.data()?.categoryId,
                    name: i.data().name,
                    nameBn: i.data().nameBn,
                    price: i.data().price,
                    createdAt: new Date(i.data().createdAt.seconds * 1000).toString()
                }
            })
            setItem(data)
        });
    }, [])

    return (
        <>
            <View>
                <ScrollView
                    style={{ maxHeight: 73, borderRadius: 5, backgroundColor: "white", padding: 5, marginTop: 5, marginBottom: 8 }}
                    horizontal={true}
                    contentContainerStyle={styles.tabBar}
                    showsHorizontalScrollIndicator={false}
                >
                    {category.map((cat: any, i: any) => (
                        <TouchableOpacity
                            key={cat.uuid}
                            onPress={() => { setIndex(i) }}
                            style={[
                                styles.tabItem,
                                index === i && styles.selectedTabItem,
                            ]}
                        >
                            <Text style={{ color: "white" }}>{cat.nameBn}</Text>
                        </TouchableOpacity>
                    ))}
                </ScrollView>
            </View>
            <View>
                <FlatList
                    data={item?.filter((item: Item) => category[index]?.uuid == (item?.categoryId as any)._key.path.segments[6])}
                    renderItem={({ item }) =>
                        <ListItem
                            containerStyle={{
                                backgroundColor: "white",
                                borderRadius: 5,
                                height: 57,
                                marginHorizontal: 8
                            }}
                            style={{ marginBottom: 7, borderRadius: 10 }}
                            onPress={() => { }}
                        >
                            <Avatar
                                icon={{
                                    name: "lunch-dining",
                                    type: "material",
                                    size: 26,
                                    color: "#5a888f"
                                }}
                            />
                            <ListItem.Content>
                                <ListItem.Title>{item.nameBn}</ListItem.Title>
                            </ListItem.Content>
                            <Text>{item.price}/=</Text>
                            <TouchableHighlight
                                activeOpacity={0.6}
                                underlayColor="#DDDDDD"
                                onPress={() => navigation.navigate("Edit Item", { data: item })}
                                style={{ padding: 15, margin: -15, marginHorizontal: -5, borderRadius: 50, backgroundColor: "#eee" }}>
                                <Entypo name="edit" size={20} color="#ffab66" />
                            </TouchableHighlight>
                        </ListItem>
                    }
                />

            </View>
        </>
    )
};


const styles = StyleSheet.create({
    scene: {
        flex: 1,
    },
    tabBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white",
        minWidth: "100%",
        borderRadius: 5,
        maxHeight: 60,
        padding: 5,
    },
    tabItem: {
        paddingVertical: 12,
        paddingHorizontal: 15,
        backgroundColor: "#ffccb5",
        marginHorizontal: 2,
        borderRadius: 5,
        alignItems: "center"
    },
    selectedTabItem: {
        backgroundColor: "#f08554",
    },

    input: {
        height: 40,
        margin: 10,
        borderWidth: 2,
        padding: 2,
        color: "red",
        width: 50,
        marginVertical: -25,
        marginHorizontal: 5,
        fontSize: 18,
        borderColor: "#ffab66",
        borderRadius: 5,
        textAlign: "center",
        fontWeight: "800"
    },
});