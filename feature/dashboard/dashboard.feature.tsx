import { useEffect, useState } from "react";
import { Text } from "@rneui/base";
import { router } from "expo-router";
import { StyleSheet, TextInput, TouchableOpacity } from "react-native";

export interface DashboardFeatureProps {

    onPress: (userName:string, password:string) => void
}

export default function DashboardFeature(props: DashboardFeatureProps) {
    const [userName, setUserName] = useState("")
    const [password, setPassword] = useState("")

    useEffect(() => {
    }, [])

    const onPress=()=>{
        props.onPress(userName, password)
    }

    return (
        <>
            <TextInput
                style={styles.input}
                onChangeText={setUserName}
                value={userName}
                placeholder="Username"
            />
            <TextInput
                style={styles.input}
                onChangeText={setPassword}
                value={password}
                placeholder="Password"
                // keyboardType="numeric"
                textContentType="password"
                secureTextEntry={true}
            />
            {/* <TouchableOpacity style={styles.button} onPress={() => Alert.alert('Simple TO pressed')}> */}
            <TouchableOpacity style={styles.button} onPress={onPress}>
                <Text style={{ color: "white" }}>SUBMIT</Text>
            </TouchableOpacity>
        </>
    )

};

const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 44 / 5
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#f7c868',
        padding: 10,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 44 / 5,
    },
});