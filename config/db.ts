
import * as  SQLiteDatabase from "expo-sqlite";
import uuid from 'react-native-uuid';

export const db = SQLiteDatabase.openDatabase('cafe102.db');

const dropSuccessMessage = (name: string) => {
    console.log(name + " dropped successfully!")
    return name + " dropped successfully!"
}

const dropFailedMessage = (name: string, error: any) => {
    console.log(name + " failed to drop!", error)
    return name + " failed to drop!"
}

const createSuccessMessage = (name: string) => {
    console.log(name + " created successfully!")
    return name + " created successfully!"
}

const createFailedMessage = (name: string, error: any) => {
    console.log(name + " failed to create!", error)
    return name + " failed to create!"
}

const insertSuccessMessage = (name: string) => {
    console.log(name + " inserted successfully!")
    return name + " inserted successfully!"
}

const insertFailedMessage = (name: string, error: any) => {
    console.log(name + " failed to insert!", error)
    return name + " failed to insert!"
}

const fetchSuccessMessage = (name: string, tx: SQLiteDatabase.SQLTransaction, res: SQLiteDatabase.SQLResultSet) => {
    console.log(name + " fetch successfully!", res.rows)
    return name + " fetch successfully!"
}

const fetchFailedMessage = (name: string, error: any) => {
    console.log(name + " failed to fetch!", error)
    return name + " failed to fetch!"
}

const dropOrganizationTableQuery = `DROP TABLE IF EXISTS Organization`;
const dropUserTableQuery = `DROP TABLE IF EXISTS User`;
const dropCategoryTableQuery = `DROP TABLE IF EXISTS Category`;
const dropItemTableQuery = `DROP TABLE IF EXISTS Item`;
const dropTokenTableQuery = `DROP TABLE IF EXISTS Token`;
const dropItemSaleTableQuery = `DROP TABLE IF EXISTS ItemSale`;
const dropExpenseTableQuery = `DROP TABLE IF EXISTS Expense`;
const dropOkTableQuery = `DROP TABLE IF EXISTS Ok`;


const createOrganizationTableQuery = `CREATE TABLE IF NOT EXISTS 
    Organization (
        uuid TEXT PRIMARY KEY,
        name TEXT,
        nameBn TEXT,
        createdAt TEXT,
        cloud BOOLEAN
    )`;

const createUserTableQuery = `CREATE TABLE IF NOT EXISTS 
    User (
        uuid TEXT PRIMARY KEY,
        name TEXT,
        nameBn TEXT,
        createdAt TEXT,
        organizationId TEXT,
        cloud BOOLEAN,
        FOREIGN KEY (organizationId) REFERENCES Organization (uuid) 
    )`;

const createCategoryTableQuery = `CREATE TABLE IF NOT EXISTS 
    Category (
        uuid TEXT PRIMARY KEY,
        name TEXT,
        nameBn TEXT,
        createdAt TEXT,
        cloud BOOLEAN
    )`;

const createItemTableQuery = `CREATE TABLE IF NOT EXISTS 
    Item (
        uuid TEXT PRIMARY KEY,
        name TEXT,
        nameBn TEXT,
        price INTEGER,
        createdAt TEXT,
        categoryId TEXT,
        cloud BOOLEAN,
        FOREIGN KEY (categoryId) REFERENCES Category (uuid) 
    )`;

const createTokenTableQuery = `CREATE TABLE IF NOT EXISTS 
    Token (
        uuid TEXT PRIMARY KEY,
        amount INTEGER,
        createdAt TEXT
        cloud BOOLEAN
    )`;

const createItemSaleTableQuery = `CREATE TABLE IF NOT EXISTS 
    ItemSale (
        uuid text PRIMARY KEY,
        price INTEGER,
        quantity INTEGER,
        createdAt TEXT,
        itemId TEXT,
        itemNameEn TEXT,
        itemNameBn TEXT,
        tokenId TEXT,
        cloud BOOLEAN,
        FOREIGN KEY (itemId) REFERENCES Item (uuid),
        FOREIGN KEY (tokenId) REFERENCES Token (uuid)
    )`;


const createExpenseTableQuery = `CREATE TABLE IF NOT EXISTS 
    Expense (
        uuid text PRIMARY KEY,
        amount INTEGER,
        createdAt TEXT,
        nameEn TEXT,
        cloud BOOLEAN,
        nameBn TEXT
    )`;

const createOkTableQuery = `CREATE TABLE IF NOT EXISTS 
    Ok (
        uuid TEXT PRIMARY KEY,
        name TEXT,
        ok BOOLEAN,
        createdAt TEXT
    )`;

export const createAllTable = () => {
    db.transaction((tx) => {
        tx.executeSql(createOrganizationTableQuery, [], () => createSuccessMessage('Organization Table'), (_, error): any => createFailedMessage('Organization Table', error));
        tx.executeSql(createUserTableQuery, [], () => createSuccessMessage('User Table'), (_, error): any => createFailedMessage('User Table', error));
        tx.executeSql(createCategoryTableQuery, [], () => createSuccessMessage('Category Table'), (_, error): any => createFailedMessage('Category Table', error));
        tx.executeSql(createItemTableQuery, [], () => createSuccessMessage('Item Table'), (_, error): any => createFailedMessage('Item Table', error));
        tx.executeSql(createTokenTableQuery, [], () => createSuccessMessage('Token Table'), (_, error): any => createFailedMessage('Token Table', error));
        tx.executeSql(createItemSaleTableQuery, [], () => createSuccessMessage('Item Sale Table'), (_, error): any => createFailedMessage('Item Sale Table', error));
        tx.executeSql(createExpenseTableQuery, [], () => createSuccessMessage('Expense Table'), (_, error): any => createFailedMessage('Expense Table', error));
        tx.executeSql(createOkTableQuery, [], () => createSuccessMessage('Ok Table'), (_, error): any => createFailedMessage('Ok Table', error));
        
        const insertOkTableCreateQuery = `INSERT INTO Ok (uuid, name, ok, createdAt) VALUES ('${uuid.v4()}','All Table Created','${true}', '${new Date()}')`
        tx.executeSql(insertOkTableCreateQuery, [], () => insertSuccessMessage('Ok Table data'), (_, error): any => insertFailedMessage('Ok Table data', error));
    });
}

export const dropAllTable = () => {
    db.transaction((tx) => {
        tx.executeSql(dropOrganizationTableQuery, [], () => dropSuccessMessage('Organization Table'), (_, error): any => dropFailedMessage('Organization Table', error));
        tx.executeSql(dropUserTableQuery, [], () => dropSuccessMessage('User Table'), (_, error): any => dropFailedMessage('User Table', error));
        tx.executeSql(dropCategoryTableQuery, [], () => dropSuccessMessage('Category Table'), (_, error): any => dropFailedMessage('Category Table', error));
        tx.executeSql(dropItemTableQuery, [], () => dropSuccessMessage('Item Table'), (_, error): any => dropFailedMessage('Item Table', error));
        tx.executeSql(dropTokenTableQuery, [], () => dropSuccessMessage('Token Table'), (_, error): any => dropFailedMessage('Token Table', error));
        tx.executeSql(dropItemSaleTableQuery, [], () => dropSuccessMessage('Item Sale Table'), (_, error): any => dropFailedMessage('Item Sale Table', error));
        tx.executeSql(dropExpenseTableQuery, [], () => dropSuccessMessage('Expense Table'), (_, error): any => dropFailedMessage('Item Sale Table', error));
        tx.executeSql(dropOkTableQuery, [], () => dropSuccessMessage('Ok Table'), (_, error): any => dropFailedMessage('Ok Table', error));
    });
}

const insertOrganizationInitialDataQuery = `INSERT INTO table_user (name, nameBn, createdAt) VALUES ('NSTU','এন.এস.টি.ইউ.', '${new Date()}', 'firebase Id qfdqFNDALK')`

export const insertInitialTable = () => {
    db.transaction((tx) => {
        tx.executeSql(insertOrganizationInitialDataQuery, [], () => insertSuccessMessage('Organization Table'), (_, error): any => insertFailedMessage('Organization Table', error));
        // tx.executeSql(dropUserTableQuery, [], () => dropSuccessMessage('User Table'), (_, error): any => dropFailedMessage('User Table', error));
    });
}

export const getOrganizationListQuery = `SELECT * FROM Organization`
export const getUserListQuery = `SELECT * FROM User`
export const getCategoryListQuery = `SELECT * FROM Category`
export const getItemListQuery = `SELECT * FROM Item`
export const getTokenListQuery = `SELECT * FROM Token`
export const getItemSaleListQuery = `SELECT * FROM ItemSale`
export const getExpenseListQuery = `SELECT * FROM Expense`

export const getCategoryList = () => {
    db.transaction((tx) => {
        tx.executeSql(getCategoryListQuery, [], (tx, res) => fetchSuccessMessage('Category list', tx, res), (_, error): any => fetchFailedMessage('Category list', error));
    });
}

const initialDbCheck = () => {
    const query = `SELECT * from Ok`
    db.transaction((tx) => {
        tx.executeSql(query, [],
            (tx, res) => {
                console.log("Local database checked. passed!")
            },
            (_, error): any => {
                if (error.message.includes('no such table:')) {
                    createAllTable();
                }
            }
        );
    });
}

export default initialDbCheck

export { }