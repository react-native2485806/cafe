import { initializeApp, } from 'firebase/app';
// import { getAnalytics } from "firebase/analytics";
// import { getAuth } from "firebase/auth";
import { CACHE_SIZE_UNLIMITED, getFirestore, initializeFirestore, memoryLocalCache } from "firebase/firestore";

// New Minitrade from 28/05/2024
const firebaseConfig = {
  apiKey: "AIzaSyDrqF6ETl0k7AaRQvPiF9H2ai0wSen1l3c",
  authDomain: "mini-trade-72e0f.firebaseapp.com",
  projectId: "mini-trade-72e0f",
  storageBucket: "mini-trade-72e0f.appspot.com",
  messagingSenderId: "578016338254",
  appId: "1:578016338254:web:f479a7322c3ac6e2867e1f",
  measurementId: "G-FJ3YHTKCMW"
};


// Traders live
// const firebaseConfig = {
//   apiKey: "AIzaSyAGZeFSPY6WWiT2WPdVnNTuv_D_pb6JFh4",
//   authDomain: "traders-921bb.firebaseapp.com",
//   projectId: "traders-921bb",
//   storageBucket: "traders-921bb.appspot.com",
//   messagingSenderId: "1087803778488",
//   appId: "1:1087803778488:web:83e3ba34cf8d50c0611b4d",
//   measurementId: "G-H9JCBQKN62"
// };

// Trade
// const firebaseConfig = {
//   apiKey: "AIzaSyACiqFMu6Mq4XMX2M-2Bbo849Yuh1TEx5Q",
//   authDomain: "trade-a79d5.firebaseapp.com",
//   projectId: "trade-a79d5",
//   storageBucket: "trade-a79d5.appspot.com",
//   messagingSenderId: "961007064133",
//   appId: "1:961007064133:web:945e3eea1ee7dbb0be7f2c",
//   measurementId: "G-Q8Y1G3H209",
// };

export const app = initializeApp(firebaseConfig);
export const firestore = getFirestore(app);
