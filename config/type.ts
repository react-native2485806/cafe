
export interface Organization {
    uuid: string,
    name: string,
    nameBn: string,
    createdAt: {
        nanoseconds: number,
        seconds: number
    }
    cloud: boolean,
}

export interface User {
    uuid: string,
    name: string,
    nameBn: string,
    userName: string,
    password: string,
    createdAt: {
        nanoseconds: number,
        seconds: number
    },
    cloud: boolean,
}


export interface Category {
    uuid: string,
    name: string,
    nameBn: string,
    createdAt: {
        nanoseconds: number,
        seconds: number
    }
    cloud: boolean,
}

export interface Item {
    uuid: string,
    name: string,
    nameBn: string,
    price: number,
    createdAt: {
        nanoseconds: number,
        seconds: number
    },
    categoryId: string,
    cloud: boolean,
}

export interface Token {
    uuid: string,
    amount: number,
    createdAt: {
        nanoseconds: number,
        seconds: number
    }
    cloud: boolean,
}

export interface ItemSale {
    uuid: string,
    itemId: string,
    categoryId?: string,
    itemNameBn: string,
    itemNameEn: string,
    tokenId: string,
    price: number,
    quantity: number,
    createdAt: {
        nanoseconds: number,
        seconds: number
    }
    cloud: boolean,
}

export interface Expense {
    uuid: string,
    title: string,
    titleBn: string,
    description: string,
    amount: number,
    createdAt: {
        nanoseconds: number,
        seconds: number
    }
}

export interface WrappedItemSale {
    itemId: string,
    itemNameBn: string,
    itemNameEn: string,
    price: number,
    quantity: number,
}

export interface SelectedMenu {
    uuid: string,
    quantity: number,
    price: number,
    name: string,
    nameBn: string,
    categoryId?: string,
}