import { addDoc, collection, deleteDoc, doc, documentId, getDocs, orderBy, query, setDoc, updateDoc, where } from "firebase/firestore";
import { firestore } from "./firebaseConfig";

const data = {
    name: "Khichuri",
    NameBn: "খিচুরি",
    categoryId: "Category/PZCJSz8SSc9FjOJfLzbd",
    order: 2,
    price: 1815,
    createdAt: new Date()
}

export const deleteData = async (collectionName: string, documentId: string) => {
    try {
        const docRef = await deleteDoc(doc(firestore, collectionName, documentId));
        console.log("docRef:  ", docRef)
        return docRef
    } catch (e) {
        console.log(e)
        return false
    }
}


export const deleteDataByRef = async (collectionName: string, refCollectionName: string, attributeName: string, attributeValue: string) => {
    try {
        const q = query(collection(firestore, refCollectionName), where(attributeName, '==', "/" + collectionName + "/" + attributeValue));
        const querySnapshot = await getDocs(q);

        querySnapshot.forEach(async (doc) => {
            console.log(`Deleting Document with id: ${doc.id}`);
            try {
                deleteData(collectionName, doc.id)
            } catch (e) {
                console.log("eeeee", e)
            }
            // // Delete each document found
            // await deleteDoc(doc.ref);
            console.log(`Document with id: ${doc.id} successfully deleted`);
        });

        return true
    } catch (e) {
        return false
    }
}

export const addData = async (collectionName: string, payload: any) => {
    try {
        const docRef = await addDoc(collection(firestore, collectionName), payload);
        return docRef
    } catch (e) {
        return false
    }
}

export const setData = async (collectionName: string, documentId: string, payload: any) => {
    try {
        const docRef = await setDoc(doc(firestore, collectionName, documentId), payload)
        console.log("docRef: ", docRef)
        return docRef
    } catch (e) {
        return false
    }
}

export const updateData = async (collectionName: string, documentId: string, payload: any) => {
    try {
        const docRef = await updateDoc(doc(firestore, collectionName, documentId), payload)
        return docRef
    } catch (e) {
        return false
    }
}

export const getData = async (collectionName: string, order: string = "createdAt", condition?: any) => {
    try {
        let q: any
        q = query(collection(firestore, collectionName));

        const querySnapshot = await getDocs(q);

        let data: any = []
        querySnapshot.docs.forEach((doc: any) => {
            data.push({ ...doc.data(), uuid: doc.id })
        });

        return data
    } catch (e) {
        return false
    }
}

type WhereOps = "<" | "<=" | "==" | "!=" | ">=" | ">" | "array-contains" | "in" | "array-contains-any" | "not-in";

export const getDataByDocumentId = async (collectionName: string, whereOps: WhereOps, condition?: any, order: string = "createdAt") => {
    try {
        let q: any
        q = query(collection(firestore, collectionName), where(documentId(), whereOps, condition), orderBy(order));

        const querySnapshot = await getDocs(q);

        let data: any = []
        querySnapshot.docs.forEach((doc: any) => {
            data.push({ ...doc.data(), uuid: doc.id })
        });

        return data
    } catch (e) {
        return false
    }
}