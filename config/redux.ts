// store.ts
import { configureStore  } from '@reduxjs/toolkit';
import catalogReducer from '../feature/catalog/redux/catalogReducer';
import loggerMiddleware from './loggerMiddleware';

const store = configureStore({
  reducer: {
    catalogReducer: catalogReducer
    // Add more reducers as needed
  },
  middleware:(getDefaultMiddleware)=> getDefaultMiddleware().concat(loggerMiddleware),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;