import { addDoc, doc, setDoc } from "firebase/firestore";
import { addData, getData, setData, updateData } from "./apis"
import { db } from "./db";
import { Category, Expense, Item, ItemSale, Organization, Token, User } from "./type";
import UUID from 'react-native-uuid';
import { firestore } from "./firebaseConfig";

export const syncFirebaseToSqliteOrganizationTable = async () => {
    const data = await getData("Organization")
    data?.forEach((element: Organization) => {
        let createdAt = new Date(element.createdAt.seconds * 1000)
        let selectQuery = `SELECT * FROM Organization WHERE Organization.uuid="${element.uuid}"`
        let insertQuery = `INSERT INTO Organization (uuid, name, nameBn, createdAt) VALUES ('${element.uuid}', '${element.name}', '${element.nameBn}', '${createdAt}')`
        db.transaction((tx) => {
            tx.executeSql(
                selectQuery, [],
                (tx, res) => {
                    if (!res.rows.length) {
                        tx.executeSql(
                            insertQuery, [],
                            (tx, res) => { },
                            (tx, error) => false
                        )
                    }
                },
                (tx, error) => false
            )
        })
    });
}

export const syncFirebaseToSqliteUserTable = async () => {
    const data = await getData("User")
    data?.forEach((element: User) => {
        let createdAt = new Date(element.createdAt.seconds * 1000)
        let selectQuery = `SELECT * FROM User WHERE User.uuid="${element.uuid}"`
        let insertQuery = `INSERT INTO User (uuid, name, nameBn, userName, password, createdAt) VALUES ('${element.name}', '${element.nameBn}', '${element.userName}', '${element.password}', '${createdAt}')`
        db.transaction((tx) => {
            tx.executeSql(
                selectQuery, [],
                (tx, res) => {
                    if (!res.rows.length) {
                        tx.executeSql(
                            insertQuery, [],
                            (tx, res) => { },
                            (tx, error) => false
                        )
                    }
                },
                (tx, error) => false
            )
        })
    });
}

export const syncFirebaseToSqliteCategoryTable = async () => {
    const data = await getData("Category")
    data?.forEach((element: Category) => {
        let createdAt = new Date(element.createdAt.seconds * 1000)
        let selectQuery = `SELECT * FROM Category WHERE Category.uuid="${element.uuid}"`
        let insertQuery = `INSERT INTO Category (uuid, name, nameBn, createdAt) VALUES ('${element.uuid}', '${element.name}', '${element.nameBn}', '${createdAt}')`
        let updateQuery = `UPDATE Category SET name='${element.name}', nameBn='${element.nameBn}' WHERE Category.uuid="${element.uuid}"`
        db.transaction((tx) => {
            tx.executeSql(
                selectQuery, [],
                (tx, res) => {
                    if (!res.rows.length) {
                        tx.executeSql(
                            insertQuery, [],
                            (tx, res) => { },
                            (_, error) => false
                        )
                    } else {
                        tx.executeSql(
                            updateQuery, [],
                            (tx, res) => {
                            },
                            (_, error) => {
                                return false
                            }
                        )
                    }
                },
                (tx, error) => false

            )
        })
    });
}

export const syncFirebaseToSqliteItemTable = async () => {
    const data = await getData("Item")
    data.forEach((element: Item, index: number) => {
        let categoryId = (element?.categoryId as any)?._key.path.segments[6]
        let createdAt = new Date(element?.createdAt.seconds * 1000)
        let selectQuery = `SELECT * FROM Item WHERE Item.uuid="${element.uuid}"`
        let insertQuery = `INSERT INTO Item (uuid, name, nameBn, price, createdAt, categoryId) VALUES ('${element?.uuid}', '${element?.name}', '${element?.nameBn}', '${element?.price}', '${createdAt}', '${categoryId}')`
        db.transaction((tx) => {
            tx.executeSql(
                selectQuery, [],
                (tx, res) => {
                    if (!res.rows.length) {
                        tx.executeSql(
                            insertQuery, [],
                            (tx, res) => { },
                            (_, error) => false
                        )
                    }
                },
                (tx, error) => false
            )
        })
    });
}

export const syncFirebaseToSqliteTokenTable = async () => {
    const data = await getData("Token")
    data?.forEach((element: Token) => {
        let createdAt = new Date(element.createdAt.seconds * 1000)
        let selectQuery = `SELECT * FROM Token WHERE Token.uuid="${element.uuid}"`
        let insertQuery = `INSERT INTO Token (uuid, amount, createdAt) VALUES ('${element.uuid}', '${element.amount}', '${createdAt}')`
        db.transaction((tx) => {
            tx.executeSql(
                selectQuery, [],
                (tx, res) => {
                    if (!res.rows.length) {
                        tx.executeSql(
                            insertQuery, [],
                            (tx, res) => { },
                            (_, error) => false
                        )
                    }
                },
                (tx, error) => false

            )
        })
    });
}


export const syncFirebaseToSqliteItemSaleTable = async () => {
    const data = await getData("ItemSale")
    data?.forEach((element: ItemSale) => {
        let itemId = (element.itemId as any)._key.path.segments[6]
        let tokenId = (element.tokenId as any)._key.path.segments[6]
        let createdAt = new Date(element.createdAt.seconds * 1000)
        let selectQuery = `SELECT * FROM ItemSale WHERE ItemSale.uuid="${element.uuid}"`
        let insertQuery = `INSERT INTO ItemSale (uuid, itemId, price, quantity, createdAt, tokenId, itemNameBn, itemNameEn) VALUES ('${element.uuid}', '${itemId}',  '${element.price}', '${element.quantity}', '${createdAt}', '${tokenId}', '${element.itemNameBn}', '${element.itemNameEn}')`

        db.transaction((tx) => {
            tx.executeSql(
                selectQuery, [],
                (tx, res) => {
                    if (!res.rows.length) {
                        tx.executeSql(
                            insertQuery, [],
                            (tx, res) => { },
                            (tx, error) => false
                        )
                    }

                },
                (tx, error) => {
                    return false
                }
            )
        })
    });
}


export const syncFirebaseToSqliteExpenseTable = async () => {
    const data = await getData("Expense")
    data?.forEach((element: Expense) => {
        let createdAt = new Date(element.createdAt.seconds * 1000)
        let selectQuery = `SELECT * FROM Expense WHERE Expense.uuid="${element.uuid}"`
        let insertQuery = `INSERT INTO Expense (uuid, amount, createdAt,nameBn, nameEn) VALUES ('${element.uuid}', '${element.amount}', '${createdAt}', '${element.titleBn}', '${element.title}')`

        db.transaction((tx) => {
            tx.executeSql(
                selectQuery, [],
                (tx, res) => {
                    if (!res.rows.length) {
                        tx.executeSql(
                            insertQuery, [],
                            (tx, res) => { },
                            (tx, error) => false
                        )
                    }

                },
                (tx, error) => {
                    return false
                }
            )
        })
    });
}

export const syncFirebaseDbToSqlite = async () => {
    console.log("Pulling . . .")
    // syncFirebaseToSqliteOrganizationTable()
    // syncFirebaseToSqliteUserTable()
    await syncFirebaseToSqliteCategoryTable()
    await syncFirebaseToSqliteItemTable()
    await syncFirebaseToSqliteTokenTable()
    await syncFirebaseToSqliteItemSaleTable()
    await syncFirebaseToSqliteExpenseTable()
}

export const syncSqliteToFirebaseOrganizationTable = async () => {
    const data = await getData("Organization")
    let selectQuery = `SELECT * FROM Organization`
    db.transaction((tx) => {
        tx.executeSql(
            selectQuery, [],
            (tx, res) => {

                res.rows._array?.forEach(async element => {
                    let dataExists = (data.filter((d: any) => d?.uuid == element?.uuid)).length
                    if (dataExists == 0) {
                        await setData("Item", element?.uuid, {
                            name: element?.name,
                            nameBn: element?.nameBn,
                            createdAt: new Date(element?.createdAt)
                        })
                    }
                    else {
                        await updateData("Item", element?.uuid, {
                            name: element?.name,
                            nameBn: element?.nameBn
                        })
                    }
                });

            },
            (tx, error) => false
        )
    })
}

export const syncSqliteToFirebaseCategoryTable = async () => {
    const data = await getData("Category")
    let selectQuery = `SELECT * FROM Category`
    db.transaction((tx) => {
        tx.executeSql(
            selectQuery, [],
            (tx, res) => {
                res.rows._array?.forEach(async element => {
                    let dataExists = (data.filter((d: any) => d?.uuid == element?.uuid)).length
                    if (dataExists == 0) {
                        await setData("Item", element?.uuid, {
                            name: element?.name,
                            nameBn: element?.nameBn,
                            createdAt: new Date(element?.createdAt)
                        })
                    }
                    else {
                        await updateData("Category", element?.uuid, {
                            name: element?.name,
                            nameBn: element?.nameBn
                        })
                    }
                });

            },
            (tx, error) => false
        )
    })
}

export const syncSqliteToFirebaseItemTable = async () => {
    const data = await getData("Item")
    let selectQuery = `SELECT * FROM Item`
    db.transaction((tx) => {
        tx.executeSql(
            selectQuery, [],
            (tx, res) => {
                res.rows._array?.forEach(async element => {
                    let dataExists = (data.filter((d: any) => d?.uuid == element?.uuid)).length
                    if (dataExists == 0) {
                        await setData("Item", element?.uuid, {
                            name: element?.name,
                            nameBn: element?.nameBn,
                            price: element?.price,
                            categoryId: doc(firestore, "/Category/" + element?.categoryId),
                            createdAt: new Date(element?.createdAt)
                        })
                    }
                    else {
                        await updateData("Item", element?.uuid, {
                            name: element?.name,
                            nameBn: element?.nameBn,
                            price: element?.price,
                            categoryId: doc(firestore, "/Category/" + element?.categoryId),
                        })
                    }
                });
            },
            (tx, error) => false
        )
    })
}


export const syncSqliteToFirebaseTokenTable = async () => {
    const data = await getData("Token")
    let selectQuery = `SELECT * FROM Token`
    db.transaction((tx) => {
        tx.executeSql(
            selectQuery, [],
            (tx, res) => {
                res.rows._array?.forEach(async element => {
                    let dataExists = (data.filter((d: any) => d.uuid == element.uuid)).length
                    if (dataExists == 0) {
                        setData("Token", element.uuid, {
                            amount: element.amount,
                            createdAt: new Date(element.createdAt)
                        })
                    }
                    else {
                        updateData("Token", element.uuid, {
                            amount: element.amount,
                        })
                    }
                });
            },
            (tx, error) => false
        )
    })
}


export const syncSqliteToFirebaseItemSaleTable = async () => {
    const data = await getData("ItemSale")
    let selectQuery = `SELECT * FROM ItemSale`
    db.transaction((tx) => {
        tx.executeSql(
            selectQuery, [],
            (tx, res) => {
                res.rows._array?.forEach(async element => {
                    let dataExists = (data.filter((d: any) => d.uuid == element.uuid)).length
                    if (dataExists == 0) {
                        setData("ItemSale", element.uuid, {
                            itemId: doc(firestore, "/Item/" + element?.itemId),
                            itemNameBn: element.itemNameBn,
                            itemNameEn: element.itemNameEn,
                            tokenId: doc(firestore, "/Token/" + element?.tokenId),
                            price: element.price,
                            quantity: element.quantity,
                            createdAt: new Date(element.createdAt)
                        })
                    }
                    else {
                        updateData("ItemSale", element.uuid, {
                            itemId: doc(firestore, "/Item/" + element?.itemId),
                            itemNameBn: element.itemNameBn,
                            itemNameEn: element.itemNameEn,
                            tokenId: doc(firestore, "/Token/" + element?.tokenId),
                            price: element.price,
                            quantity: element.quantity,
                        })
                    }
                });
            },
            (tx, error) => false
        )
    })
}


export const syncSqliteToFirebaseExpenseTable = async () => {
    const data = await getData("Expense")
    let selectQuery = `SELECT * FROM Expense`
    db.transaction((tx) => {
        tx.executeSql(
            selectQuery, [],
            (tx, res) => {
                res.rows._array?.forEach(async element => {
                    let dataExists = (data.filter((d: any) => d.uuid == element.uuid)).length
                    if (dataExists == 0) {
                        setData("Expense", element.uuid, {
                            nameBn: element.nameBn,
                            nameEn: element.nameEn,
                            amount: element.amount,
                            createdAt: new Date(element.createdAt)
                        })
                    }
                    else {
                        updateData("Expense", element.uuid, {
                            nameBn: element.nameBn,
                            nameEn: element.nameEn,
                            amount: element.amount,
                            quantity: element.quantity,
                        })
                    }
                });
            },
            (tx, error) => false
        )
    })
}


export const syncSqliteToFirebaseDb = async () => {
    console.log("Pushing . . .")
    await syncSqliteToFirebaseOrganizationTable()
    // await syncFirebaseToSqliteUserTable()
    // await syncFirebaseToSqliteCategoryTable()
    await syncSqliteToFirebaseItemTable()
    await syncSqliteToFirebaseTokenTable()
    await syncSqliteToFirebaseItemSaleTable()
    await syncSqliteToFirebaseExpenseTable()
    return true
}

export { }